import { convertDirection, directionToName } from '../enums/Direction';
import AbstractComponent from './AbstractComponent';

class GameBoard extends AbstractComponent {
    /**
     * Game board component constructor
     * @param socketReceiving
     * @param socketEmitting
     * @param options
     */
    constructor(socketReceiving, socketEmitting, options) {
        super(socketReceiving, socketEmitting, options);

        this.loadAssets();

        /**
         * All information needed to render the game board
         */
        this.game = {};

        this.canvas = null;

        this.keyboardInputsLocked = false;

        window.addEventListener('resize', () => {
            this.findCanvasOptimalDimension();
            if (this.game.board.radius === undefined) {
                this.renderAll();
                return;
            }
            this.renderLocal();
        });
    }

    loadAssets() {
        const image = {
            WALL: '/img/wall.png',
            EMPTY: '/img/boden.png',
            FINAL: '/img/schild.png',
            PLAYER0: '/img/eule_blau.png',
            PLAYER0_LEFT: '/img/eule_blau_links.png',
            PLAYER0_RIGHT: '/img/eule_blau_rechts.png',
            PLAYER0_BACK: '/img/eule_blau_hinten.png',
            PLAYER1: '/img/eule_pink.png',
            ITEM_BOX: '/img/itembox.png',
        };

        this.images = {};
        this.images.player = [{}, {}];

        const promiseList = [];
        promiseList.push(new Promise((resolve) => {
            this.images.wall = new Image();
            this.images.wall.src = image.WALL;
            this.images.wall.addEventListener('load', () => resolve());
        }));

        promiseList.push(new Promise((resolve) => {
            this.images.empty = new Image();
            this.images.empty.src = image.EMPTY;
            this.images.empty.addEventListener('load', () => resolve());
        }));

        promiseList.push(new Promise((resolve) => {
            this.images.final = new Image();
            this.images.final.src = image.FINAL;
            this.images.final.addEventListener('load', () => resolve());
        }));

        promiseList.push(new Promise((resolve) => {
            this.images.player[0].forward = new Image();
            this.images.player[0].forward.src = image.PLAYER0;
            this.images.player[0].forward.addEventListener('load', () => resolve());
        }));

        promiseList.push(new Promise((resolve) => {
            this.images.player[0].left = new Image();
            this.images.player[0].left.src = image.PLAYER0_LEFT;
            this.images.player[0].left.addEventListener('load', () => resolve());
        }));

        promiseList.push(new Promise((resolve) => {
            this.images.player[0].right = new Image();
            this.images.player[0].right.src = image.PLAYER0_RIGHT;
            this.images.player[0].right.addEventListener('load', () => resolve());
        }));

        promiseList.push(new Promise((resolve) => {
            this.images.player[0].back = new Image();
            this.images.player[0].back.src = image.PLAYER0_BACK;
            this.images.player[0].back.addEventListener('load', () => resolve());
        }));

        promiseList.push(new Promise((resolve) => {
            this.images.itemF = new Image();
            this.images.itemF.src = image.ITEM_BOX;
            this.images.itemF.addEventListener('load', () => resolve());
        }));


        Promise.all(promiseList).then(() => {
            if (this.game.board.radius === undefined) {
                this.renderAll();
                return;
            }
            this.renderLocal();
        });
    }

    update(data) {
        this.updateGameData(data);
    }

    updateGameData(data) {
        this.game = data;
        if (this.game.board.radius === undefined) {
            this.renderAll();
            return;
        }
        this.renderLocal();
    }

    /**
     * Create game board ui
     * @param elements {canvas, name}
     */
    createUi(elements) {
        this.canvas = elements.canvas;
        this.canvas_initialized = false;
    }

    /**
     * This method set the optimal dimension for the canvas
     * It is called every time the window is resized
     */
    findCanvasOptimalDimension() {
        if (this.canvas === null) {
            return;
        }

        // set content height to windows height - navbar height
        // todo fixme should get dimension in createUI
        const content = document.querySelector('.content');
        const canvasParent = document.querySelector('.item_board');

        content.style.height = `${window.innerHeight}px`;

        const maxBoardDimension = Math.min(
            window.innerHeight,
            canvasParent.clientWidth,
        );

        const boardWidth = this.game.board.dimensions[0];
        const boardHeight = this.game.board.dimensions[1];
        const dim = Math.min(boardWidth, boardHeight);
        const scale = Math.floor(maxBoardDimension / dim);

        this.canvas.width = scale * dim;
        this.canvas.height = scale * dim;
    }

    drawRotatedImage(image, x, y, width, height, direction, initialRotation) {
        const degrees = convertDirection(direction) + initialRotation;

        this.ctx.save();
        this.ctx.translate(x + width / 2, y + height / 2);
        this.ctx.rotate(degrees * Math.PI / 180);
        this.ctx.drawImage(image, -width / 2, -height / 2, width, height);
        this.ctx.restore();
    }

    drawOwlImage(playerNum, x, y, width, height, direction) {
        this.ctx.save();
        this.ctx.translate(x + width / 2, y + height / 2);
        this.ctx.drawImage(
            this.images.player[playerNum][directionToName(direction)],
            -width / 2,
            -height / 2,
            width,
            height,
        );
        this.ctx.restore();
    }

    renderAll() {
        if (!this.canvas_initialized) {
            this.findCanvasOptimalDimension();
            this.ctx = this.canvas.getContext('2d');
        }

        const tilePerCanvas = Math.max(
            this.game.board.dimensions[0],
            this.game.board.dimensions[1],
        );

        const offset = { x: 0, y: 0 };
        const tile = {
            height: this.canvas.height / tilePerCanvas,
            width: this.canvas.width / tilePerCanvas,
        };

        this.render(offset, tile);
    }

    renderLocal() {
        if (!this.canvas_initialized) {
            this.findCanvasOptimalDimension();
            this.ctx = this.canvas.getContext('2d');
        }


        // create useful const var
        const canvas = {
            height: this.canvas.height,
            width: this.canvas.width,
        };

        const tilePerCanvas = Math.min(
            this.game.board.dimensions[0],
            this.game.board.dimensions[1], 10,
        );

        const tile = {
            height: canvas.height / tilePerCanvas,
            width: canvas.width / tilePerCanvas,
        };

        const game = {
            height: this.game.board.height * tile.height,
            width: this.game.board.width * tile.width,
        };

        const playerBoardPosition = {
            x: this.game.state.player.position[0],
            y: this.game.state.player.position[1],
        };

        const playerCanvasPosition = {
            x: playerBoardPosition.x * tile.width,
            y: playerBoardPosition.y * tile.height,
        };

        const offset = {
            x: playerCanvasPosition.x - canvas.width / 2,
            y: playerCanvasPosition.y - canvas.height / 2,
        };

        // make sure offset is not < 0
        if (offset.x < 0) {
            offset.x = 0;
        }
        if (offset.y < 0) {
            offset.y = 0;
        }

        // make sure offset is not > game dimension
        if (offset.x + canvas.width > game.width) {
            offset.x = game.width - canvas.width;
        }
        if (offset.y + canvas.height > game.height) {
            offset.y = game.height - canvas.height;
        }
        this.render(offset, tile);
    }

    render(offset, tile) {
        // clear canvas
        this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);

        const off = 0.05;
        const offsetAsset = tile.width * off;

        // render way
        for (let x = 0; x < this.game.board.dimensions[0]; x++) {
            for (let y = 0; y < this.game.board.dimensions[1]; y++) {
                const pos = {
                    x: tile.width * x - offset.x,
                    y: tile.height * y - offset.y,
                };
                this.ctx.drawImage(this.images.empty, pos.x, pos.y, tile.width, tile.height);
            }
        }

        // draw upper border
        if (offset.y === 0) {
            for (let x = 0; x < this.game.board.dimensions[0]; x++) {
                if (this.game.board.walls[0][x]) {
                    this.ctx.drawImage(
                        this.images.wall,
                        tile.width * x - offset.x,
                        tile.height * (-1 / 2) - offset.y,
                        tile.width - offsetAsset,
                        tile.height - offsetAsset,
                    );
                }

                if (x === this.game.board.dimensions[0]
                    || this.game.board.walls[0][x]) {
                    this.ctx.drawImage(
                        this.images.wall,
                        tile.width * (x + 1 / 2),
                        tile.height * (-1 / 2),
                        tile.width - offsetAsset,
                        tile.height - offsetAsset,
                    );
                }
            }
        }

        // draw left border
        if (offset.x === 0) {
            for (let y = 0; y < this.game.board.dimensions[1]; y++) {
                if (this.game.board.walls[y][0]) {
                    this.ctx.drawImage(
                        this.images.wall,
                        tile.width * (-1 / 2),
                        tile.height * y,
                        tile.width - offsetAsset,
                        tile.height - offsetAsset,
                    );
                }

                if (y === this.game.board.dimensions[1]
                    || this.game.board.walls[y][0]) {
                    this.ctx.drawImage(
                        this.images.wall,
                        tile.width * (-1 / 2),
                        tile.height * (y + 1 / 2),
                        tile.width - offsetAsset,
                        tile.height - offsetAsset,
                    );
                }
            }

            if (this.game.board.walls[0][0]) {
                this.ctx.drawImage(
                    this.images.wall,
                    tile.width * -1 / 2,
                    tile.width * -1 / 2,
                    tile.width - offset,
                    tile.height - offset,
                );
            }
        }

        // render wall
        for (let y = 0; y < this.game.board.dimensions[1]; y++) {
            const row = this.game.board.walls[y];
            for (let x = 0; x < this.game.board.dimensions[0]; x++) {
                const pos = {
                    x: tile.width * x - offset.x,
                    y: tile.height * y - offset.y,
                };

                if (row[x] && (pos.x + 2 * tile.width) >= 0 && (pos.y + 2 * tile.height) >= 0) {
                    this.ctx.drawImage(
                        this.images.wall,
                        pos.x + offsetAsset / 2,
                        pos.y + offsetAsset / 2,
                        tile.width - offsetAsset,
                        tile.height - offsetAsset,
                    );

                    // draw right
                    const borderX = x === this.game.board.dimensions[0] - 1;
                    const bef = borderX || row[x + 1];
                    if (bef) {
                        this.ctx.drawImage(
                            this.images.wall,
                            pos.x + 1 / 2 * tile.width,
                            pos.y,
                            tile.width - offsetAsset,
                            tile.height - offsetAsset,
                        );
                    }

                    // draw down
                    const borderY = y === this.game.board.dimensions[1] - 1;
                    if (borderY || this.game.board.walls[y + 1][x]) {
                        this.ctx.drawImage(
                            this.images.wall,
                            pos.x,
                            pos.y + 1 / 2 * tile.height,
                            tile.width - offsetAsset,
                            tile.height - offsetAsset,
                        );

                        if (borderX || borderY
                            || (bef && this.game.board.walls[y + 1][x + 1])) {
                            this.ctx.drawImage(
                                this.images.wall,
                                pos.x + 1 / 2 * tile.width,
                                pos.y + 1 / 2 * tile.height,
                                tile.width - offsetAsset,
                                tile.height - offsetAsset,
                            );
                        }
                    }
                }
            }
        }

        // draw goal(s)
        this.game.board.goal_fields.forEach((goal) => {
            this.ctx.drawImage(this.images.final, tile.width * goal[0] - offset.x,
                tile.height * goal[1] - offset.y, tile.width, tile.height);
        });

        // draw itemboxes
        const itemFields = this.game.board.item_fields;
        if (itemFields !== undefined) {
            itemFields.forEach((itemF) => {
                this.ctx.drawImage(
                    this.images.itemF,
                    tile.width * itemF[0],
                    tile.height * itemF[1],
                    tile.width,
                    tile.height,
                );
            });
        }


        if (this.game.state !== undefined) {
            // draw marks
            const { marks } = this.game.state;
            if (marks !== undefined) {
                marks.forEach((mark) => {
                    this.ctx.drawImage(
                        this.images.mark,
                        tile.width * mark[0],
                        tile.height * mark[1],
                        tile.width,
                        tile.height,
                    );
                });
            }

            // draw arrows
            const { positions } = this.game.state;
            if (positions !== undefined) {
                positions.forEach((position) => {
                    this.drawRotatedImage(
                        this.images.arrow,
                        position[0] * tile.width,
                        position[1] * tile.height,
                        tile.width,
                        tile.height,
                        position[2],
                        -90,
                    );
                });
            }

            // draw player(s)
            const i = 0;
            // Object.values(this.game.players).forEach((player) => {
            const { player } = this.game.state;
            const pos = {
                x: tile.width * player.position[0] - offset.x,
                y: tile.height * player.position[1] - offset.y,
            };

            this.drawOwlImage(
                i,
                pos.x + offsetAsset / 2,
                pos.y + offsetAsset / 2,
                tile.width - offsetAsset,
                tile.height - offsetAsset,
                player.direction,
            );

            // draw shadow
            if (player.radius !== undefined) {
                for (let y = 0; y < this.game.board.dimensions[1]; y++) {
                    for (let x = 0; x < this.game.board.dimensions[0]; x++) {
                        const playerPos = this.game.state.player.position;
                        if (Math.abs(playerPos[0] - x)
                            + Math.abs(playerPos[1] - y) > player.radius) {
                            const tilePos = {
                                x: x * tile.width - offset.x,
                                y: y * tile.width - offset.y,
                            };
                            if (tilePos.x >= 0
                                && tilePos.y >= 0
                                && tilePos.x < this.canvas.width
                                && tilePos.y < this.canvas.height) {
                                this.ctx.fillRect(
                                    tilePos.x,
                                    tilePos.y,
                                    tile.width,
                                    tile.height,
                                );
                            }
                        }
                    }
                }
            }
        }
    }
}

export { GameBoard as default };
