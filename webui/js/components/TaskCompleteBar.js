import AbstractComponent from './AbstractComponent';

/**
 * Map Selector class
 * Responsible for the editor and some ui buttons
 */
class TaskCompleteBar extends AbstractComponent {
    setup(b) {
        if (b) {
            this.element.classList.add('bg-success');
            this.element.classList.remove('bg-danger');
            this.element.innerHTML = 'task is complete';
        } else {
            this.element.classList.remove('bg-success');
            this.element.classList.add('bg-danger');
            this.element.innerHTML = 'task is not complete';
        }
    }

    createUi(e) {
        this.element = e;
        this.setup(false);
    }

    update(data) {
        const s = data.state;
        const t = s === undefined ? undefined : s.task;
        if (t !== undefined) {
            this.setup(t.completed);
            const r = t.reason;
            if (r !== undefined && r !== '') {
                this.element.innerHTML += ` (${r})`;
            }
        } else {
            this.setup(false);
        }
    }
}

export default TaskCompleteBar;
