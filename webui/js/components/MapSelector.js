import AbstractComponent from './AbstractComponent';

/**
 * Map Selector class
 * Responsible for the editor and some ui buttons
 */
class MapSelector extends AbstractComponent {
    constructor(socketEmitting, socketReceiving, options) {
        super(socketEmitting, socketReceiving, options);
        this.mapSelect = null;
    }

    createUi(elements) {
        this.element = elements.select;
        this.element.innerHTML = `
<select class="form-control" id="map_select">
</select>
        `;
        this.mapSelect = document.getElementById('map_select');
        if (!this.options) {
            this.mapSelect.disabled = true;
        } else {
            this.mapSelect.addEventListener('change', () => {
                const selection = {
                    map: {
                        action: 'choose',
                        name: this.mapSelect.value,
                    },
                };
                this.socketEmitting.emit('input', selection);
            });
        }
    }

    addMap(map, selected = false) {
        const el = document.createElement('option');
        el.innerText = map;
        if (selected) {
            el.selected = true;
        }
        this.mapSelect.appendChild(el);
    }

    update(data) {
        this.name = data.board.name;
        this.mapSelect.innerHTML = '';
        if (this.options !== undefined) {
            this.options.forEach((map) => {
                if (map === data.board.name) {
                    this.addMap(map, true);
                } else {
                    this.addMap(map);
                }
            });
        } else {
            this.addMap(data.board.name);
        }
    }
}

export default MapSelector;
