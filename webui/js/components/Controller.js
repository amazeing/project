import { ActionEnum, Action } from '../enums/Action';
import AbstractComponent from './AbstractComponent';

class Controller extends AbstractComponent {
    createUi(options) {
        // setup html
        options.parent.innerHTML = `
        <div class="joystick-grid">
            <div></div>
            <div class="grid-btn round-corner click-cursor w" id="move" title="Move forward"><div class="g-icon w"></div></div>
            <div></div>
            <div class="grid-btn round-corner click-cursor l" id="turn_left" title="Turn left"><div class="g-icon l"></div></div>
            <div class="grid-btn"></div>
            <div class="grid-btn round-corner click-cursor r" id="turn_right" title="Turn right"><div class="g-icon r"></div></div>
        </div>`;

        // setup gui event listener
        document.getElementById('turn_right')
            .addEventListener('click', () => this.performAction(ActionEnum.TURN_RIGHT));

        document.getElementById('turn_left')
            .addEventListener('click', () => this.performAction(ActionEnum.TURN_LEFT));

        document.getElementById('move')
            .addEventListener('click', () => this.performAction(ActionEnum.MOVE));

        // create some button click listeners
        document.querySelector('body')
            .addEventListener('keydown', e => this.keyControl(e));
    }

    performAction(action) {
        Action.send(action, this.socketEmitting, this.options.playerId);
    }

    keyControl(event) {
        const ARROW_LEFT = 37;
        const ARROW_UP = 38;
        const ARROW_RIGHT = 39;
        const KEY_A = 65;
        const KEY_W = 87;
        const KEY_D = 68;

        if (this.keyboardInputsLocked) {
            return;
        }

        if (event.which === ARROW_UP || event.which === KEY_W) {
            this.performAction(ActionEnum.MOVE);
        } else if (event.which === ARROW_LEFT || event.which === KEY_A) {
            this.performAction(ActionEnum.TURN_LEFT);
        } else if (event.which === ARROW_RIGHT || event.which === KEY_D) {
            this.performAction(ActionEnum.TURN_RIGHT);
        }
    }

    update() {
        super.update();
    }
}

export { Controller as default };
