import Controller from './Controller';
import GameBoard from './GameBoard';
import Log from './Log';
import Input from './Input';
import MapSelector from './MapSelector';
import TaskCompleteBar from './TaskCompleteBar';
import PrettyPrintDisplay from './PrettyPrintDisplay';

class ComponentsBuilder {
    constructor(socketEmitting, socketReceiving, playerId) {
        this.socketEmitting = socketEmitting;
        this.socketReceiving = socketReceiving;
        this.gameBoard = null;
        this.log = null;
        this.input = null;
        this.controller = null;
        this.taskButton = null;
        this.playerId = playerId;
        this.mapSelector = null;
        this.taskCompleteBar = null;
        this.speed = 70;
        this.prettyPrint = null;
    }

    getSpeed() {
        return (100 - this.speed) * 25;
    }

    setSpeed(s) {
        this.speed = s;
    }

    addPrettyPrintDisplay() {
        this.prettyPrint = new PrettyPrintDisplay(this.socketEmitting, this.socketReceiving, {});
        this.prettyPrint.createUi({
            box: document.getElementById('component-print-wrap'),
            popUp: this.popUp,
        });
    }

    addSlider() {
        const wrap = document.getElementById('component-slider-wrap');
        wrap.innerHTML = `<div class="slidecontainer mt-3 form-group row mt-2">
            <label class="col-sm-2 col-form-row">Speed: </label>
            <div class="col-sm-10">
                <input type="range" min="1" max="100" value="${this.speed}" class="slider" id="component-speed-slider">
            </div>
        </div>`;

        const slider = document.getElementById('component-speed-slider');

        const ev = new CustomEvent('SliderChanged');
        document.addEventListener('SliderChanged', () => this.setSpeed(parseInt(slider.value, 10)));
        slider.oninput = () => document.dispatchEvent(ev);
    }

    popUp(str, color) {
        let style = '';
        if (color !== undefined) style = `background: ${color}; border-color: ${color};`;

        const el = document.createElement('div');
        const code = `
            <div style="position: absolute; left: 50%; top: 40%;" id="component-show-msg">
                <div style="position: relative; left: -50%; top: -50%; ${style}" id="saved-msg">
                    ${str}
                </div>
            </div>`;
        el.classList.add('show-msg');
        el.insertAdjacentHTML('beforeend', code);
        el.addEventListener('animationend', () => document.body.removeChild(el));
        document.body.appendChild(el);
    }

    addTaskCompleteBar() {
        this.taskCompleteBar = new TaskCompleteBar(this.socketEmitting, this.socketReceiving, {});
        this.taskCompleteBar.createUi(document.getElementById('component-task-complete'));
    }

    addGameBoard(data, zoom = true) {
        this.gameBoard = new GameBoard(this.socketEmitting, this.socketReceiving, { zoom });
        this.gameBoard.createUi({
            canvas: document.getElementById('component-gameboard-canvas'),
            name: document.getElementById('component-gameboard-name'),
        });
    }

    addInput() {
        if (this.gameBoard === null) {
            throw new Error('A game board should be added before the input component');
        }
        this.input = new Input(this.socketEmitting, this.socketReceiving, {
            dependencies: {
                gameBoard: this.gameBoard,
            },
        });
        this.input.createUi({
            editor: document.getElementById('component-input-editor'),
            buttons: document.getElementById('component-input-buttons'),
            popUp: this.popUp,
            speedGetter: this,
        });
    }

    addController() {
        this.controller = new Controller(this.socketEmitting, this.socketReceiving, {
            playerId: this.playerId,
        });
        this.controller.createUi({
            parent: document.getElementById('component-controller'),
        });
    }

    addLog() {
        this.log = new Log(this.socketEmitting, this.socketReceiving, {});
        this.log.createUi({
            log: document.getElementById('component-log'),
            buttons: document.getElementById('component-log-buttons'),
            popUp: this.popUp,
            speedGetter: this,
        });
    }

    addTaskButton() {
        this.taskButton = document.getElementById('component-task');
        this.taskButton.innerHTML = `
            <div id="component-task-check" class="btn click-cursor btn-success">Check task</div>
            <div id="component-task-reset" class="btn click-cursor btn-primary">Reset task</div>
        `;
    }

    addMapSelector(opt) {
        this.mapSelector = new MapSelector(this.socketEmitting, this.socketReceiving, opt);
        this.mapSelector.createUi({
            select: document.getElementById('component-map-selector'),
        });
    }

    appendErrElement(msg) {
        const alert = document.createElement('div');
        alert.classList.add('alert');
        alert.classList.add('alert-danger');
        alert.innerHTML = `
${msg}
<button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
</button>`;
        alert.querySelector('button')
            .onclick = () => alert.remove();
        document.querySelector('body')
            .insertAdjacentElement('afterbegin', alert);
    }

    /**
     * Setup method
     * Should be called in the end to create the layout
     * and add a event listener
     */
    setup() {
        if (this.taskButton === null) {
            const elToDelete = document.getElementById('component-task');
            elToDelete.parentNode.removeChild(elToDelete);
        } else {
            document.getElementById('component-task-check')
                .onclick = () => this.socketEmitting.emit('input', { task: 'check' });

            document.getElementById('component-task-reset')
                .onclick = () => this.socketEmitting.emit('input', { task: 'reset' });
        }


        if (this.taskCompleteBar === null) {
            const elToDelete = document.getElementById('component-task-complete');
            elToDelete.parentNode.removeChild(elToDelete);
        }

        if (this.controller === null && this.log === null
            && this.taskButton === null && this.mapSelector === null) {
            document.getElementById('component-control').remove();
        } else if (this.input === null) {
            document.querySelectorAll('.editor-container, #component-input-buttons')
                .forEach(el => el.remove());
            document.getElementById('component-control')
                .classList.add('col-11');
            document.getElementById('component-control')
                .classList.add('h-90');
        }

        if (this.log === null) {
            document.getElementById('component-log').remove();
        }

        if (this.controller === null) {
            document.getElementById('component-controller').remove();
        }

        this.socketReceiving.on('error', (msg) => {
            this.appendErrElement(msg.message);
        });

        this.socketReceiving.on('render', (data) => {
            document.getElementById('component-map-name').innerHTML = data.name;

            if (data.error !== undefined) {
                this.appendErrElement(data.error.message);
            } else {
                if (this.gameBoard !== null) {
                    this.gameBoard.update(data);
                }

                if (this.log !== null) {
                    this.log.update(data);
                }

                if (this.input !== null) {
                    this.input.update(data);
                }

                if (this.mapSelector !== null) {
                    this.mapSelector.update(data);
                }

                if (this.taskCompleteBar !== null) {
                    this.taskCompleteBar.update(data);
                }

                if (this.prettyPrint !== null) {
                    this.prettyPrint.update(data);
                }
            }
        });
    }
}

export { ComponentsBuilder as default };
