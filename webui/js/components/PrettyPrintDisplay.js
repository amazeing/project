import AbstractComponent from './AbstractComponent';

class PrettyPrintDisplay extends AbstractComponent {
    msg() {}

    createUi(elements) {
        this.element = elements.box;
        this.element.innerHTML = `
<div class="pt-3 scroll fancy-text fancier-text" id="component-print"></div>
<div class="mt-2" style="width: 109%;" >
    <div id="save-location" style="float: left;"></div>
    <div style="float: right;"> 
        <div class="btn btn-primary click-cursor" title="save map print" id="btn-print-save">
            <i class="fa fa-save"></i>
        </div>
    </div>
</div>`;

        const resize = () => {
            const control = document.getElementById('control').offsetHeight;
            const wrap = document.getElementById('board-wrap').offsetWidth;
            const pr = document.getElementById('component-print');
            pr.style.height = `${window.innerHeight - control - 90}px`;
            pr.style.width = `${window.innerWidth - wrap - 30}px`;
        };
        resize();
        window.addEventListener('resize', () => resize());

        this.msg = elements.popUp;
        document.getElementById('btn-print-save').addEventListener('click', () => {
            this.socketEmitting.emit('input', {
                map: {
                    action: 'save',
                    name: 'deprecated',
                },
            });
            this.msg('saved print');
        });
    }

    update(data) {
        const p = data.prettyPrint;
        if (p !== undefined) {
            document.getElementById('component-print').innerHTML = p.replace(/\n/g, '<br>');
        }
    }
}

export default PrettyPrintDisplay;
