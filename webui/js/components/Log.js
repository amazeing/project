import AbstractComponent from './AbstractComponent';

/**
 * Log class
 * Responsible for the editor and some ui buttons
 */
class Log extends AbstractComponent {
    msg() {}

    getSpeed() {}

    createUi(elements) {
        this.element = elements.log;
        this.buttons = elements.buttons;
        this.options.speedGetter = elements.speedGetter;
        this.buttons.innerHTML = `
<div class="btn btn-primary click-cursor" title="save Log" id="btn-log-save">
    <i class="fa fa-download"></i>
</div>
<div class="btn btn-primary click-cursor" title="load Log" id="btn-log-load">
    <i class="fa fa-upload"></i>
</div>`;
        this.msg = elements.popUp;
        this.buttons.classList.add('pt-2');
        const resizeScroll = () => {
            const control = document.getElementById('control').offsetHeight;
            document.getElementById('component-log')
                .style.height = `${window.innerHeight - control - this.buttons.offsetHeight - 90}px`;
        };
        document.getElementById('btn-log-save').addEventListener('click', () => {
            this.socketEmitting.emit('input', { log: 'save' });
            this.msg('Log saved.');
        });
        document.getElementById('btn-log-load').addEventListener('click', () => {
            this.socketEmitting.emit('input', { log: 'load' });
        });
        resizeScroll();
        window.addEventListener('resize', () => resizeScroll());
    }

    update(data) {
        this.element.innerHTML = `<div id="component-msg">${data.log.text.replace(/\n/g, '<br>')}</div>`;
        const msg = document.getElementById('component-msg');
        this.element.scrollTop = msg.offsetHeight + msg.offsetTop;
        const c = data.log.continues;
        if (c !== undefined && c) {
            setTimeout(() => this.socketEmitting.emit('input', { log: 'step' }), this.options.speedGetter.getSpeed());
        }
    }
}

export default Log;
