/**
 * Base class for all components
 */
class AbstractComponent {
    /**
     *
     * @param socketEmitting
     * @param socketReceiving
     * @param options
     */
    constructor(socketEmitting, socketReceiving, options) {
        if (new.target === AbstractComponent) {
            throw new TypeError('Cannot construct AbstractComponent instances directly');
        }
        this.socketEmitting = socketEmitting;
        this.socketReceiving = socketReceiving;
        this.options = options;
        this.registerDependencies();
    }

    /**
     * Create ui for this component
     */
    createUi() {
        throw new Error('TODO implement me!');
    }

    /**
     * Register component dependencies
     */
    registerDependencies() { }


    /**
     * Called by socket.on('render')
     */
    update() { }
}

export { AbstractComponent as default };
