import * as acemodule from 'ace-builds/src-noconflict/ace';
import * as themeTwilight from 'ace-builds/src-noconflict/theme-twilight';
import AceGrammar from '../../aceJs/ace_grammar';
import CustomModeJson from '../../aceJs/custom_grammar.json';
import AbstractComponent from './AbstractComponent';

/**
 * Input class
 * Responsible for the editor and some ui buttons
 */
class Input extends AbstractComponent {
    /**
     * Log constructor
     * @param socketEmitting
     * @param socketReceiving
     * @param options
     */
    constructor(socketEmitting, socketReceiving, options) {
        super(socketEmitting, socketReceiving, options);

        this.currentLineOfMarker = 0;

        this.playing = false;
        this.speedGetter = null;
        this.oldProg = '';
    }

    createUi(elements) {
        this.speedGetter = elements.speedGetter;
        const customMode = AceGrammar.getMode(CustomModeJson);

        // enable user-defined code folding in the specification (new feature)
        customMode.supportCodeFolding = true;

        // enable syntax lint-like validation in the grammar
        customMode.supportGrammarAnnotations = true;

        // enable user-defined autocompletion (if defined)
        customMode.supportAutoCompletion = true;
        customMode.autocompleter.options = { prefixMatch: true, caseInsensitiveMatch: false };
        // or for context-sensitive autocompletion, extracted from the grammar
        customMode.autocompleter.options = {
            prefixMatch: true,
            caseInsensitiveMatch: false,
            inContext: true,
        };
        // or for dynamic (context-sensitive) autocompletion, extracted from user actions
        customMode.autocompleter.options = {
            prefixMatch: true, caseInsensitiveMatch: false, inContext: true, dynamic: true,
        };

        // setup editor
        this.editor = acemodule.edit('component-input-editor');
        this.editor.setTheme(themeTwilight);
        // this.editor.session.setMode(customMode);
        this.editor.getSession().setOptions({ useWorker: false });

        this.setMarkerToLine(this.currentLineOfMarker);

        // setup log buttons
        elements.buttons.innerHTML = `
            <button class="btn btn-round play" id="play_action" title="play">&#9654;</button>
            <button class="btn btn-round step" id="step_action" title="step">&#8615;</button>
            <button class="btn btn-round undo" id="undo_action" title="undo">&#8612;</button>
            <button class="btn btn-round save" id="save_action" title="save program"><i class="fa fa-download" aria-hidden="true"></i></button>
            <button class="btn btn-round save" id="update_action" title="update program"><i class="fa fa-sync" aria-hidden="true"></i></button>
            <button class="btn btn-round save" id="load_action" title="load program"><i class="fa fa-upload" aria-hidden="true"></i></button>
        `;

        this.fileSelector = document.createElement('input');
        this.fileSelector.setAttribute('type', 'file');
        this.fileSelector.style.display = 'none';
        this.setEventListener();
        elements.buttons.appendChild(this.fileSelector);

        this.msg = elements.popUp;
    }

    setEventListener() {
        document.getElementById('save_action')
            .addEventListener('click', () => this.save());

        document.getElementById('load_action')
            .addEventListener('click', () => this.load());

        document.getElementById('play_action')
            .addEventListener('click', () => this.play());

        document.getElementById('step_action')
            .addEventListener('click', () => this.step());

        document.getElementById('undo_action')
            .addEventListener('click', () => this.undo());

        document.getElementById('update_action')
            .addEventListener('click', () => this.updateAction());

        this.fileSelector.addEventListener('click', (event) => {
            event.target.value = '';
        });
        this.fileSelector.addEventListener('change', () => {
            const file = this.fileSelector.files[0];
            const fileReader = new FileReader();
            fileReader.onload = (e) => {
                this.editor.session.getDocument().setValue(e.target.result);
            };
            fileReader.readAsText(file);
        });
    }

    registerDependencies() {
        this.gameBoard = this.options.dependencies.gameBoard;
    }

    newProgram(p) {
        const doc = this.editor.session.getDocument();
        const typed = doc.getValue();
        if (p === '') {
            if (typed !== this.oldProg) this.msg('Program wasn\'t compiled.', 'red');
        } else {
            doc.setValue(p);
        }
        this.oldProg = typed;
    }

    update(data) {
        if (data.program.line !== undefined) {
            this.setMarkerToLine(data.program.line);
        } else {
            this.removeMarker();
        }
        this.newProgram(data.program.text);
        if (this.playing && data.program.line !== undefined) {
            setTimeout(() => {
                this.socketEmitting.emit('input', { program: { action: 'step' } });
            }, this.speedGetter.getSpeed());
        } else if (this.playing) {
            this.play();
        }
    }

    setMarkerToLine(line) {
        if (line < 0 || line > this.editor.session.getLength()) {
            if (line === -1) {
                this.removeMarker();
                return;
            }
            throw new Error(`invalid line ${line}`);
        }

        this.removeMarker();

        const { Range } = acemodule.require('ace/range');
        this.editor.session.addMarker(new Range(line, 0, line, 1), 'ace_highlight-marker', 'fullLine');
        this.currentLineOfMarker = line;
    }


    removeMarker() {
        const markers = this.editor.session.getMarkers();

        Object.entries(markers).forEach((entry) => {
            const key = entry[0];
            const value = entry[1];

            if (value.clazz === 'ace_highlight-marker') {
                this.editor.session.removeMarker(key);
            }
        });
    }

    play() {
        this.playing = !this.playing;

        const playButton = document.getElementById('play_action');
        if (this.playing) {
            playButton.innerHTML = '&#9632;';

            // change color use undo color
            playButton.classList.remove('play');
            playButton.classList.add('undo');
            this.step();
        } else {
            playButton.innerHTML = '&#9654;';

            // change color
            playButton.classList.remove('undo');
            playButton.classList.add('play');
        }
    }

    step() {
        this.socketEmitting.emit('input', { program: { action: 'step' } });
    }

    undo() {
        this.socketEmitting.emit('input', { program: { action: 'back' } });
    }

    msg() {}

    save() {
        this.socketEmitting.emit('input', { program: { action: 'save' } });
        this.msg('Program saved.');
    }

    updateAction() {
        this.socketEmitting.emit('input', { program: { action: 'update', text: this.editor.session.getDocument().getValue() } });
    }

    load() {
        this.socketEmitting.emit('input', { program: { action: 'load' } });
    }
}

export { Input as default };
