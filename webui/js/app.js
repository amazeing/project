import io from 'socket.io-client';
import '../css/app.scss';
import ComponentsBuilder from './components/ComponentsBuilder';

function show(dom) {
    dom.classList.remove('d-none');
}

function hide(dom) {
    dom.classList.add('d-none');
}

function isLocal(hostname) {
    return hostname.endsWith('localhost') || hostname === '127.0.0.1';
}

function scheme(hostname) {
    if (isLocal(hostname)) {
        return 'http';
    }
    return 'https';
}

function port(hostname) {
    if (isLocal(hostname)) {
        return ':5000';
    }
    return '';
}

const tid = document.getElementById('tid')
    .getAttribute('data-tid');
const hostname = document.getElementById('hostname')
    .getAttribute('data-hostname');

const socketTask = io(`${scheme(hostname)}://${hostname}${port(hostname)}/tasks`);

const connectionAlert = document.getElementById('connectionAlert');

socketTask.on('connect', () => {
    const playerId = socketTask.io.engine.id;
    hide(connectionAlert);
    socketTask.emit('start', tid);
    socketTask.on('controls', (data) => {
        // build ui
        const componentsBuilder = new ComponentsBuilder(socketTask, socketTask, playerId);

        componentsBuilder.addGameBoard(data);

        if (data.log !== undefined) {
            componentsBuilder.addSlider();
            componentsBuilder.addLog();
        } else if (data.program !== undefined) {
            componentsBuilder.addLog();
        }

        if (data.program !== undefined) {
            componentsBuilder.addInput();
        }

        if (data.player !== undefined) {
            componentsBuilder.addController();
        }

        if (data.task !== undefined) {
            componentsBuilder.addTaskButton();
            componentsBuilder.addTaskCompleteBar();
        }

        if (data.print !== undefined) {
            componentsBuilder.addPrettyPrintDisplay();
        }

        componentsBuilder.addMapSelector(data.map);


        if (data.name !== undefined) {
            componentsBuilder.addName(data.name);
        }

        componentsBuilder.setup();
    });
});

socketTask.on('disconnect', () => {
    show(connectionAlert);
});

