import data from '../data/credits.json';

const creditSpace = document.getElementById('credit-space');
let max = 0;

data.entries.forEach((entry) => {
    creditSpace.innerHTML += `<h1>${entry.category}:</h1>`;
    creditSpace.innerHTML += '<br>';
    creditSpace.innerHTML += `<p>${entry.description}</p>`;
    creditSpace.innerHTML += '<br>';
    entry.names.forEach((name) => {
        const h2 = document.createElement('h2');
        h2.innerHTML = `⋅ ${name}`;
        creditSpace.insertAdjacentElement('beforeend', h2);
        max = Math.max(max, h2.offsetWidth);
        creditSpace.innerHTML += '<br>';
    });
    creditSpace.innerHTML += '<br>';
});

creditSpace.style.width = `${max}px`;
