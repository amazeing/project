/**
 * Action enum
 * @type {Readonly<{TURN_RIGHT: number, MOVE: number, TURN_LEFT: number}>}
 */
const ActionEnum = Object.freeze({
    MOVE: 0,
    TURN_LEFT: 1,
    TURN_RIGHT: 2,
});

class Action {
    /**
     * Convert action to string
     * @param action
     * @returns {string}
     */
    static toString(action) {
        switch (action) {
        case ActionEnum.MOVE:
            return 'move';

        case ActionEnum.TURN_LEFT:
            return 'turn_left';

        case ActionEnum.TURN_RIGHT:
            return 'turn_right';

        default:
            throw new TypeError('Invalid action');
        }
    }

    /**
     * Send action to server
     * @param action
     * @param socket
     */
    static send(action, socket) {
        socket.emit('input', { player: Action.toString(action) });
    }
}


export { ActionEnum, Action };
