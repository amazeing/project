/**
 * Direction enums
 * @type {Readonly<{NORTH: number, WEST: number, SOUTH: number, EAST: number}>}
 */
const Direction = Object.freeze({
    NORTH: 0,
    EAST: 90,
    SOUTH: 180,
    WEST: 270,
});

const convertDirection = (dir) => {
    switch (dir) {
    case 'north':
        return Direction.NORTH;
    case 'east':
        return Direction.EAST;
    case 'south':
        return Direction.SOUTH;
    case 'west':
        return Direction.WEST;
    default:
        throw new Error(`invalid direction ${dir}`);
    }
};

const directionToName = (dir) => {
    switch (dir) {
    case 'south':
        return 'forward';
    case 'west':
        return 'left';
    case 'east':
        return 'right';
    case 'north':
        return 'back';
    default:
        throw new Error('Direction can\'t be converted.');
    }
};


export { Direction as default, convertDirection, directionToName };
