const Encore = require('@symfony/webpack-encore');

Encore
    // directory where compiled assets will be stored
    .setOutputPath('../server/server/static')
    // public path used by the web server to access the output path
    .setPublicPath('/')

    .addEntry('app', './js/app.js')
    .addEntry('menu', './js/menu.js')
    .addEntry('credits', './js/credits.js')

    // will require an extra script tag for runtime.js
    // but, you probably want this, unless you're building a single-page app
    .enableSingleRuntimeChunk()

    .cleanupOutputBeforeBuild()
    .enableSourceMaps(!Encore.isProduction())
    .enableSassLoader()

    .addLoader({
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'eslint-loader',
        options: {
            fix: true,
        },
    })
    // .enableVersioning(Encore.isProduction())

    // copy images
    .copyFiles({
        from: './img',
        context: './',
    })

    // copy style sheets
    .copyFiles({
        from: './css',
        context: './',
        pattern: /\.css$/,
    })

    // copy fonts
    .copyFiles({
        from: './fonts',
        context: './',
    })

    // copy html files
    .copyFiles({
        from: './',
        pattern: /\.html$/,
        includeSubdirectories: false,
    });

module.exports = Encore.getWebpackConfig();
