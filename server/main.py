#! /usr/bin/env python3

import argparse

from server import config

if __name__ == '__main__':
    config.app.config['ENV'] = 'development'

    parser = argparse.ArgumentParser()
    parser.add_argument('--online', action='store_true')

    args = parser.parse_args()
    if args.online:
        config.task_events.set_online()

    config.sio.run(config.app, host='0.0.0.0')
