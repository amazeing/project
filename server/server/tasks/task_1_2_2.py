import server.model.program
from server.model import task, log
from server.model.board import text_to_board

board_text = '''
XXXXXXXXXXXXXXX
XS____________X
XXX_XXXXXXXX_XX
X____X________X
XX_X___XXXXX_XX
XX_X_XXX______X
X__X_XX__X_X_XX
XX____XX_X_X__X
X__X_XXX_XXXX_X
X_XX_X_XXXX_XXX
X_X__X_XX____XX
X_X_XX____XX__X
X_XGX__XXXXX_XX
X_____XX______X
XXXXXXXXXXXXXXX
'''


class Task(task.Task):
    def __init__(self) -> None:
        super().__init__(
            'Day 1 - Task 2.2',
            self.check_goal,
            text_to_board('Day 1 - Task 2.2', board_text)
        )
        self.enable_program_controls(server.model.program.Program('task_1_2_2'), 1)
        self.enable_log_controls(log.Log('task_1_2_2'))
