import os
from typing import Tuple

from server.model import task
from server.model.player import Player

ref_path = os.path.realpath(os.path.join(os.path.dirname(__file__), 'resources'))


class Task(task.Task):
    def __init__(self) -> None:
        super().__init__(
            'Day 3 - Task 3',
            self.check_goal,
        )
        self.enable_map_controls()
        self.enable_print_controls()
        self.player = Player(self.board)
        self.map_save_path = os.path.realpath(os.path.join(os.path.dirname(__file__), '../impl/labSaves'))

        self.setup()

    def check_goal(self) -> Tuple[bool, str]:
        name = self.board.name + '.lab'

        def read(path):
            with open(path) as file:
                return file.read().strip().upper()

        try:
            ref = read(os.path.join(ref_path, name))
            written = read(os.path.join(self.map_save_path, name))
        except FileNotFoundError:
            return False, 'file not found: {}'.format(os.path.join(self.map_save_path, name))

        if ref != written:
            ref_lines = ref.splitlines()
            written_lines = written.splitlines()
            if len(ref_lines) != len(written_lines):
                return False, 'number of lines differ; {} vs. {}'.format(len(ref_lines), len(written_lines))

            for i, (r, w) in enumerate(zip(ref_lines, written_lines)):
                if r != w:
                    if len(r) != len(w):
                        return False, 'number of characters differ in line {}; {} vs. {}'.format(i + 1, len(r), len(w))

                    for j, (c1, c2) in enumerate(zip(r, w)):
                        if c1 != c2:
                            return False, 'differs at line {}, character {}; {} vs. {}'.format(i + 1, j + 1, c1, c2)

        return True, ''

    def setup(self):
        import server.impl.print_labyrinth
        from importlib import reload
        impl = reload(server.impl.print_labyrinth)

        result = None
        try:
            result = self.timeout_fn(impl.print_labyrinth, self.board)
        except:
            import sys
            self.input_error('caught exception: {}'.format(sys.exc_info()[0]))

        if result is not None:
            self.print_result = result
        else:
            self.input_error('print_labyrinth did not return a result')

    def reset_state(self, fail=False):
        super().reset_state()
        if not fail:
            self.setup()
