import server.model.program
from server.model import task, log
from server.model.board import text_to_board

board_text = '''
XXXXXXXXXXXXXXX
XSXX____X_X_X_X
X____XX_X_____X
X_XX__XXX_X_X_X
X__XX__XX_X_X_X
X_XXXX__XXX_X_X
X___XX_XXX____X
XX_XX__X_X_X_XX
XX____XX_X_XXXX
X__XX_______XXX
X_XX_GXX_XX__XX
X__XX_____XX__X
X_XX__X_X__X_XX
X____XX_XX____X
XXXXXXXXXXXXXXX
'''


class Task(task.Task):
    def __init__(self) -> None:
        super().__init__(
            'Day 1 - Task 2.1',
            self.check_goal,
            text_to_board('Day 1 - Task 2.1', board_text)
        )
        self.enable_program_controls(server.model.program.Program('task_1_2_1'), 1)
        self.enable_log_controls(log.Log('task_1_2_1'))
