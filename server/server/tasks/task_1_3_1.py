from server.model import task, log, movement
from server.model.board import text_to_board
from server.model.execution import Execution
from server.model.player import Player

board_text = '''
XXXXXXXXXXXXXXX
XS____________X
X_XX_XXXXXX_X_X
X___________X_X
X_X_X_XX_XX_X_X
X_X_XXXX_X____X
X_XXXX___X_XX_X
X_XX_XXX_X_XX_X
X____X_____X__X
X_XX_X_XXX_XGXX
X____X_XX____XX
X_XX_X____XX__X
X______XX_XXX_X
X_X_X_XX______X
XXXXXXXXXXXXXXX
'''


class Task(task.Task):
    def __init__(self) -> None:
        super().__init__(
            'Day 1 - Task 3.1',
            self.check_goal,
            text_to_board('Day 1 - Task 3.1', board_text)
        )
        self.enable_log_controls(log.Log('task_1_3_1'))
        self.player = Player(self.board, radius=2)

        import server.impl.day_1_task_3_1
        from importlib import reload
        impl = reload(server.impl.day_1_task_3_1)

        self.timeout_fn(impl.entry)
        self.log.instructions = movement.calls
        movement.calls = []
        self.log.stepping = True
        self.log.line = 0
        self.log.step(Execution(self.player))
