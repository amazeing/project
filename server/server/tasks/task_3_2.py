import os
from typing import Tuple

from server.model import task
from server.model.player import Player


class Task(task.Task):
    def __init__(self) -> None:
        super().__init__(
            'Day 3 - Task 2',
            self.check_goal,
        )
        self.disable_task_controls()
        self.enable_map_controls()
        self.player = Player(self.board)
        self.map_load_path = os.path.realpath(os.path.join(os.path.dirname(__file__), '../impl/myLabs'))

        self.map_name = 'Test'
        self.setup()

    def check_goal(self) -> Tuple[bool, str]:
        return True, ''

    def setup(self):
        import server.impl.parse_labyrinth
        from importlib import reload
        impl = reload(server.impl.parse_labyrinth)

        board = None
        try:
            board = self.timeout_fn(impl.parse_labyrinth, os.path.join(self.map_load_path, self.map_name + '.lab'))
        except:
            import sys
            self.input_error('caught exception: {}'.format(sys.exc_info()[0]))

        if board is not None:
            self.board = board
            if self.player is not None:
                self.player.reset(self.board)
        else:
            self.input_error('parse_labyrinth did not return a board')

    def reset_state(self, fail=False):
        super().reset_state()
        if not fail:
            self.setup()
