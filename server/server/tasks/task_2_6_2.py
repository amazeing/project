from server.model import task, log, algorithms
from server.model.execution import Execution
from server.model.player import Player


class Task(task.Task):
    def __init__(self) -> None:
        super().__init__(
            'Day 2 - Task 6.2',
            self.check_goal,
        )
        self.enable_log_controls(log.Log('task_2_6_2'))
        self.enable_map_controls()
        self.player = Player(self.board)

        self.setup()

    def setup(self):
        algorithms.setup(Execution(Player(self.board)))

        import server.impl.pledge
        from importlib import reload
        impl = reload(server.impl.pledge)

        self.timeout_fn(impl.pledge)
        self.log.instructions = algorithms.calls
        algorithms.calls = []
        self.log.stepping = True
        self.log.line = 0
        self.log.step(Execution(self.player))

    def reset_state(self, fail=False):
        super().reset_state()
        if not fail:
            self.setup()
