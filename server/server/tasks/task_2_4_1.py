import server.model.program
from server.model import task, log


class Task(task.Task):
    def __init__(self) -> None:
        super().__init__(
            'Day 2 - Task 4.1',
            self.check_goal,
        )
        self.enable_program_controls(server.model.program.Program('wall_follower'))
        self.enable_log_controls(log.Log('task_2_4_1'))
        self.enable_map_controls()
