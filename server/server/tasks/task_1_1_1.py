from server.model import task, log
from server.model.board import text_to_board

board_text = '''
XXXXXXXXXX
X________X
X_XXXXXX_X
X_X____X_X
X_X_XXGX_X
X_X_XXXX_X
X_X______X
X_XXXXXXXX
X_______SX
XXXXXXXXXX
'''


class Task(task.Task):
    def __init__(self) -> None:
        super().__init__(
            'Day 1 - Task 1.1',
            self.check_goal,
            text_to_board('Day 1 - Task 1.1', board_text)
        )
        self.enable_player_controls(1)
        self.enable_log_controls(log.Log('task_1_1_1'))
