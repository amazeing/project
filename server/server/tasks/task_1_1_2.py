from server.model import task, log
from server.model.board import text_to_board

board_text = '''
XXXXXXXXXXXXXXX
XSX___________X
X___X_XX_XXX_XX
X_X_X_________X
X_X_X_XXXXXX_XX
X_X_____XX____X
X_X_X_X_XG_XX_X
X_XXXXX_X_XXX_X
X_XX_XX_XXX___X
X_X______XXX_XX
X_X_XXXX_____XX
X______XXXXX__X
X_XXXXXX___XX_X
X________X____X
XXXXXXXXXXXXXXX
'''


class Task(task.Task):
    def __init__(self) -> None:
        super().__init__(
            'Day 1 - Task 1.2',
            self.check_goal,
            text_to_board('Day 1 - Task 1.2', board_text)
        )
        self.enable_player_controls(1)
        self.enable_log_controls(log.Log('task_1_1_2'))
