from server.model import task, log, movement
from server.model.board import text_to_board
from server.model.execution import Execution
from server.model.player import Player

board_text = '''
XXXXXXXXXXXXXXX
XS________X_X_X
X_XXXXXXX____GX
X_________X_X_X
X_X_XXXXX_X_X_X
X_X_X_______X_X
X_X___X_XX_XX_X
X_XX_XX_XX_XX_X
X_______X_____X
X_XX_X_XX_XXX_X
X_X__XXXXXXX__X
X_X_XXX_XX_XX_X
X___X_X_X_____X
X_X_____X_X_X_X
XXXXXXXXXXXXXXX
'''


class Task(task.Task):
    def __init__(self) -> None:
        super().__init__(
            'Day 1 - Task 3.2',
            self.check_goal,
            text_to_board('Day 1 - Task 3.2', board_text)
        )
        self.enable_log_controls(log.Log('task_1_3_2'))
        self.player = Player(self.board, radius=2)

        import server.impl.day_1_task_3_2
        from importlib import reload
        impl = reload(server.impl.day_1_task_3_2)

        self.timeout_fn(impl.entry)
        self.log.instructions = movement.calls
        movement.calls = []
        self.log.stepping = True
        self.log.line = 0
        self.log.step(Execution(self.player))
