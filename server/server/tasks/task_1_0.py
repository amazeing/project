from server.model import task
from server.model.board import text_to_board

board_text = '''
XXXXX
XSXGX
X_X_X
X___X
XXXXX
'''


class Task(task.Task):
    def __init__(self) -> None:
        super().__init__(
            'Day 1 - Task 0',
            self.check_goal,
            text_to_board('Day 1 - Task 0', board_text)
        )
        self.enable_player_controls()
