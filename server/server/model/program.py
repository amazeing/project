import os
from typing import List, Any, Dict, Union, Optional

from server.model.instructions import Instruction, Label
from server.util.instruction_parser import InstructionParser


class Program:
    name: str
    file_path: Union[bytes, str]
    line: Optional[int]
    instructions: List[Instruction]
    parser: InstructionParser
    directory: Union[bytes, str] = os.path.realpath(os.path.join(os.path.dirname(__file__), '../impl/programs'))

    def __init__(self, name: str) -> None:
        if not os.path.exists(self.directory):
            os.makedirs(self.directory)

        self.name = name
        self.file_path = os.path.join(Program.directory, self.name + '.pro')
        self.line = None
        self.instructions = []
        self.parser = InstructionParser('move', 'turn', 'branch', 'jump', 'store', 'test', 'call', 'return')

        self.labels = {}

    def parse(self, text: str) -> None:
        self.instructions = self.parser.parse(text)
        self.line = None
        for i, instr in enumerate(self.instructions):
            if isinstance(instr, Label):
                self.labels[instr.name] = i

    def dump(self) -> str:
        return '\n'.join(str(inst) for inst in self.instructions)

    def step(self, execution) -> List[Instruction]:
        if self.line is None:
            self.line = 0

        if self.line >= len(self.instructions):
            return []

        instrs = execution.do()

        return instrs

    def back(self, execution) -> List[Instruction]:
        if self.line is None:
            return []

        if self.line >= len(self.instructions):
            return []

        instrs = execution.undo()
        if len(execution.trace) == 0:
            self.line = None

        return instrs

    def add(self, instruction: Instruction) -> None:
        self.instructions.append(instruction)

    def label(self, label: str) -> int:
        return self.labels[label]

    def __str__(self) -> str:
        return '\n'.join(str(self.instructions))

    def update(self, text: str) -> None:
        self.instructions = self.parser.parse(text)
        self.line = None
        for i, instr in enumerate(self.instructions):
            if isinstance(instr, Label):
                self.labels[instr.name] = i

    def from_file(self) -> None:
        if not os.path.isfile(self.file_path):
            self.parse('')
            return

        with open(self.file_path) as file:
            self.parse(file.read())

    def to_file(self) -> None:
        with open(self.file_path, 'w') as file:
            file.write(self.dump())

    def render(self) -> Dict[str, Any]:
        result = {
            'text': '\n'.join(str(inst) for inst in self.instructions)
        }

        if self.line is not None:
            result['line'] = self.line
        elif len(self.instructions) > 0:
            result['line'] = 0

        return result
