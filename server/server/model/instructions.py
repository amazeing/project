from typing import Dict, Any, Tuple, List

from server.model.execution import Execution


class Instruction:
    def do(self, execution: Execution) -> Any:
        raise NotImplementedError

    def undo(self, execution: Execution) -> Any:
        raise NotImplementedError

    def __str__(self) -> str:
        raise NotImplementedError


class Empty(Instruction):
    def do(self, execution: Execution) -> Any:
        execution.program.line += 1

    def undo(self, execution: Execution) -> Any:
        execution.program.line -= 1

    def __str__(self) -> str:
        return ''


class Label(Instruction):
    name: str

    def __init__(self, name: str) -> None:
        self.name = name

    def do(self, execution: Execution) -> Any:
        execution.program.line += 1

    def undo(self, execution: Execution) -> Any:
        execution.program.line -= 1

    def __str__(self) -> str:
        return self.name + ':'


class Move(Instruction):
    def do(self, execution: Execution) -> Any:
        execution.player.move()
        if execution.program is None:
            return
        execution.program.line += 1

    def undo(self, execution: Execution) -> Any:
        execution.player.turn_left()
        execution.player.turn_left()
        execution.player.move()
        execution.player.turn_left()
        execution.player.turn_left()
        if execution.program is None:
            return
        execution.program.line -= 1

    def __str__(self) -> str:
        return 'move'


class Turn(Instruction):
    direction: str

    def __init__(self, direction: str) -> None:
        self.direction = direction

    def do(self, execution: Execution) -> Any:
        if self.direction == 'left':
            execution.turn_left()
        else:
            execution.turn_right()
        if execution.program is None:
            return
        execution.program.line += 1

    def undo(self, execution: Execution) -> Any:
        if self.direction == 'right':
            execution.turn_left()
        else:
            execution.turn_right()
        if execution.program is None:
            return
        execution.program.line -= 1

    def __str__(self) -> str:
        return 'turn ' + self.direction


class Store(Instruction):
    subject: str

    def __init__(self, subject: str) -> None:
        self.subject = subject

    def do(self, execution: Execution) -> Any:
        if self.subject == 'direction':
            execution.store_direction()
        execution.program.line += 1

    def undo(self, execution: Execution) -> Any:
        if self.subject == 'direction':
            execution.delete_direction()
        execution.program.line -= 1

    def __str__(self) -> str:
        return 'store ' + self.subject


class Test(Instruction):
    args: Tuple[str, ...]

    def __init__(self, *args: str) -> None:
        self.args = args

    def do(self, execution: Execution) -> Any:
        if self.args[0] == 'direction':
            execution.store_test(execution.turn_counter[-1] == 0)

        if self.args[0] == 'wall':
            execution.store_test(execution.player.is_wall_in_direction(self.args[1]))

        execution.program.line += 1

    def undo(self, execution: Execution) -> Any:
        execution.delete_test()
        execution.program.line -= 1

    def __str__(self) -> str:
        return 'test ' + ' '.join([str(a) for a in self.args])


class Jump(Instruction):
    def __init__(self, label: str) -> None:
        self.label = label

    def do(self, execution: Execution) -> Any:
        execution.remember_branch()
        execution.program.line = execution.program.label(self.label)

    def undo(self, execution: Execution) -> Any:
        execution.program.line = execution.forget_branch()

    def __str__(self) -> str:
        return 'jump ' + self.label


class Branch(Instruction):
    def __init__(self, label_true, label_false: str) -> None:
        self.label_true = label_true
        self.label_false = label_false

    def do(self, execution: Execution) -> Any:
        execution.remember_branch()
        if execution.test_results[-1]:
            execution.program.line = execution.program.label(self.label_true)
        else:
            execution.program.line = execution.program.label(self.label_false)

    def undo(self, execution: Execution) -> Any:
        execution.program.line = execution.forget_branch()

    def __str__(self) -> str:
        return 'branch ' + self.label_true + ' ' + self.label_false


class Call(Instruction):
    def __init__(self, function: str) -> None:
        self.function = function

    def do(self, execution: Execution) -> Any:
        position = execution.player.position
        execution.add_player_state(position[0], position[1], execution.player.direction)

        def run():
            from server.model.program import Program
            prog = Program(self.function)
            prog.from_file()
            inner_execution = Execution(execution.player, prog, True)
            for inst in prog.step(inner_execution):
                execution.executed.append(inst)
            while prog.line < len(prog.instructions):
                for inst in prog.step(inner_execution):
                    execution.executed.append(inst)

        def handler():
            raise TimeoutError("timeout")

        import signal
        if not execution.child:
            signal.signal(signal.SIGALRM, handler)
            signal.alarm(1)

        try:
            run()
        except TimeoutError:
            execution.program.line = len(execution.program.instructions)
            return
        finally:
            if not execution.child:
                signal.alarm(0)

        execution.program.line += 1

    def undo(self, execution: Execution) -> Any:
        x, y, direction = execution.remove_player_state()
        execution.player.position = (x, y)
        execution.player.direction = direction
        execution.program.line -= 1

    def __str__(self) -> str:
        return 'call ' + self.function


class Return(Instruction):
    def do(self, execution: Execution) -> Any:
        execution.program.line = len(execution.program.instructions)

    def undo(self, execution: Execution) -> Any:
        pass

    def __str__(self) -> str:
        return 'return'


class Builder:
    labels: Dict[str, Instruction]
    instructions: List[Instruction]

    def __init__(self) -> None:
        self.labels = {}
        self.instructions = []

    def reset(self) -> None:
        self.labels = {}
        self.instructions = []

    def empty(self) -> None:
        self.instructions.append(Empty())

    def label(self, name: str) -> None:
        instruction = Label(name)
        self.instructions.append(instruction)
        self.labels[name] = instruction

    def move(self) -> None:
        self.instructions.append(Move())

    def turn(self, direction: str) -> None:
        self.instructions.append(Turn(direction))

    def store(self, subject: str) -> None:
        self.instructions.append(Store(subject))

    def test(self, *args: str) -> None:
        self.instructions.append(Test(*args))

    def jump(self, label: str) -> None:
        self.instructions.append(Jump(label))

    def branch(self, label_true, label_false: str) -> None:
        self.instructions.append(Branch(label_true, label_false))

    def call(self, function: str) -> None:
        self.instructions.append(Call(function))

    def return_instr(self) -> None:
        self.instructions.append(Return())

    def finish(self) -> List[Instruction]:
        from server.events import InputError

        for instr in self.instructions:
            if isinstance(instr, Jump) and instr.label not in self.labels:
                raise InputError('undefined label')
            if isinstance(instr, Branch) and \
                    (instr.label_true not in self.labels or instr.label_false not in self.labels):
                raise InputError('undefined label')

        return self.instructions
