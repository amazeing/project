from unittest import TestCase
from unittest.mock import MagicMock, patch

from server.model.task import Task


class TestTask(TestCase):
    def setUp(self):
        self.task = Task('test', lambda: (False, 'empty task'))

    def assert_error(self):
        result = self.task.render()
        self.assertIn('error', result)
        self.assertIn('message', result['error'])
        result = self.task.render()
        self.assertNotIn('error', result)

    def test_empty(self):
        from server.model.board import empty

        self.assertIs(empty, self.task.board)
        self.assertDictEqual({'task': {}}, self.task.controls())

        self.assertIsNone(self.task.log)
        self.assertIsNone(self.task.player)
        self.assertIsNone(self.task.program)

        self.assertFalse(self.task.completed)
        self.assertIsNone(self.task.error)
        self.assertIsNone(self.task.state)

        self.assertIn('task', self.task.controls_handler)
        self.assertIn('log', self.task.controls_handler)
        self.assertIn('player', self.task.controls_handler)
        self.assertIn('program', self.task.controls_handler)
        self.assertIn('map', self.task.controls_handler)

        self.task.input(None)

        mock_render = MagicMock(return_value='test')
        self.task.board.render = mock_render
        result = self.task.render()
        self.assertDictEqual({'name': 'test', 'board': 'test'}, result)
        mock_render.assert_called_once_with()

    def test_invalid_input(self):
        self.task.ctrls = ['task', 'log', 'player', 'program', 'map']

        tests = [
            {},
            {'task': '1', 'log': '2'},
            {'test': '1'},
            {'task': 'test'},
            {'log': 'test'},
            {'player': 'test'},
            {'program': 'test'},
            {'program': {'action': 'update'}},
            {'program': {'action': 'load', 'key': 'value'}},
            {'program': {'action': 'test'}},
            {'map': 'test'},
            {'map': {'action': 'update'}},
            {'map': {'action': 'load'}},
            {'map': {'action': 'save'}},
            {'map': {'action': 'update', 'key': 'value'}},
        ]

        for t in tests:
            self.task.input(t)
            self.assert_error()

    def test_input_task(self):
        self.task.input({'task': 'check'})
        result = self.task.render()
        self.assertIn('state', result)
        self.assertIn('task', result['state'])
        self.assertIn('completed', result['state']['task'])
        self.assertIn('reason', result['state']['task'])
        self.assertFalse(result['state']['task']['completed'])
        result = self.task.render()
        self.assertNotIn('state', result)

        self.task.objective_fn = lambda: (True, '')
        mock = MagicMock()
        self.task.input({'task': 'check'})
        self.assertTrue(self.task.completed)
        mock.reset_mock()
        result = self.task.render()
        self.assertIn('state', result)
        self.assertIn('task', result['state'])
        self.assertIn('completed', result['state']['task'])
        self.assertTrue(result['state']['task']['completed'])
        result = self.task.render()
        self.assertIn('state', result)
        self.assertIn('task', result['state'])
        self.assertIn('completed', result['state']['task'])
        self.assertTrue(result['state']['task']['completed'])

        self.task.input({'task': 'reset'})
        self.assertFalse(self.task.completed)
        result = self.task.render()
        self.assertIn('state', result)
        self.assertIn('task', result['state'])
        self.assertIn('completed', result['state']['task'])
        self.assertIn('reason', result['state']['task'])
        self.assertFalse(result['state']['task']['completed'])
        result = self.task.render()
        self.assertNotIn('state', result)

    def test_input_log(self):
        from server.model.log import Log
        mock_log = MagicMock(spec=Log)
        self.task.enable_log_controls(mock_log)
        self.assertIn('log', self.task.controls())

        result = self.task.render()
        mock_log.render.assert_called_once_with()
        self.assertIn('log', result)

        self.task.input({'log': 'load'})
        mock_log.from_file.assert_called_once_with()

        self.task.input({'log': 'save'})
        mock_log.to_file.assert_called_once_with()

    def test_input_player(self):
        from server.model.board import Board
        from server.model.player import Player
        self.task.board = Board('', (1, 1), [[False]], [(0, 0)], [], [], [])
        self.task.enable_player_controls()
        self.assertIn('player', self.task.controls())
        mock_player = MagicMock(spec=Player)
        self.task.player = mock_player

        result = self.task.render()
        mock_player.render.assert_called_once_with()
        self.assertIn('state', result)
        self.assertIn('player', result['state'])

        self.task.input({'player': 'move'})
        mock_player.move.assert_called_once_with()

        self.task.input({'player': 'turn_left'})
        mock_player.turn_left.assert_called_once_with()

        self.task.input({'player': 'turn_right'})
        mock_player.turn_right.assert_called_once_with()

    def test_input_program(self):
        from server.model.board import Board
        from server.model.program import Program
        self.task.board = Board('', (1, 1), [[False]], [(0, 0)], [], [], [])
        mock_program = MagicMock(spec=Program)
        self.task.enable_program_controls(mock_program)
        self.task.enable_log_controls(MagicMock())
        self.assertIn('program', self.task.controls())

        result = self.task.render()
        mock_program.render.assert_called_once_with()
        self.assertIn('program', result)

        self.task.input({'program': {'action': 'update', 'text': 'test'}})
        mock_program.update.assert_called_once_with('test')

        self.task.input({'program': {'action': 'step'}})
        mock_program.step.assert_called_once_with(self.task.execution)

        self.task.input({'program': {'action': 'back'}})
        mock_program.back.assert_called_once_with(self.task.execution)

        self.task.input({'program': {'action': 'load'}})
        mock_program.from_file.assert_called_once_with()

        self.task.input({'program': {'action': 'save'}})
        mock_program.to_file.assert_called_once_with()

    def test_input_map(self):
        from server.model.board import Board
        self.task.enable_map_controls()
        self.assertIn('map', self.task.controls())
        mock_board = MagicMock(spec=Board)
        mock_board.name = 'name'
        self.task.board = mock_board

        result = self.task.render()
        mock_board.render.assert_called_once_with()
        self.assertIn('board', result)

        with patch('server.model.board.Board.choose') as mock_choose:
            mock_choose.return_value = mock_board
            self.task.input({'map': {'action': 'choose', 'name': 'test'}})
            mock_choose.assert_called_once_with('test')
