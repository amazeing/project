import os
import signal
from typing import Dict, Any, Callable, Optional, List, Tuple

from server.model.board import empty, Board
from server.model.execution import Execution
from server.model.instructions import Move, Turn
from server.model.log import Log
from server.model.player import Player
from server.model.program import Program


class Task:
    name: str
    ctrls: List[str]
    board: Board
    log: Optional[Log]
    player: Optional[Player]
    program: Optional[Program]
    execution: Optional[Execution]
    controls_handler: Dict[str, Callable[[Any, Any], None]]

    def __init__(self, name,
                 objective_fn: Callable[[], Tuple[bool, str]],
                 board: Board = empty) -> None:
        super().__init__()

        self.name = name
        self.objective_fn = objective_fn
        self.ctrls = ['task']
        self.board = board

        self.log = None
        self.player = None
        self.program = None
        self.execution = None

        self.completed = False
        self.error = None
        self.state = None

        self.map_name = self.board.name
        self.map_load_path = None
        self.map_save_path = None
        self.print_result = None

        self.controls_handler = {
            'task': self.handle_task,
            'log': self.handle_log,
            'player': self.handle_player,
            'program': self.handle_program,
            'map': self.handle_map,
        }

    def input_error(self, reason: str) -> None:
        self.error = {'message': 'invalid input: ' + reason}

    def add_state(self, **kwargs):
        if self.state is None:
            self.state = {}
        self.state = {**self.state, **kwargs}

    def reset_state(self, fail: bool = False):
        self.completed = False
        if self.player is not None:
            self.player.reset(self.board)
        if self.log is not None:
            self.log.parse('')
        if self.execution is not None:
            self.execution.reset()

    def controls(self):
        boards = Board.boards()
        result = {ctrl: {} for ctrl in self.ctrls}

        if 'map' in result:
            if not self.map_load_path:
                result['map'] = boards
            else:
                result['map'] = [os.path.splitext(f)[0] for f in os.listdir(self.map_load_path) if f.endswith('.lab')]

        return result

    def enable_log_controls(self, log: Log) -> None:
        self.log = log
        self.ctrls.append('log')

    def enable_player_controls(self, radius: Optional[int] = None) -> None:
        self.player = Player(self.board, radius=radius)
        self.ctrls.append('player')

    def enable_program_controls(self, program: Program, radius: Optional[int] = None) -> None:
        self.player = Player(self.board, radius=radius)
        self.program = program
        self.execution = Execution(self.player, program)
        self.ctrls.append('program')

    def enable_map_controls(self) -> None:
        self.ctrls.append('map')

    def enable_print_controls(self) -> None:
        self.ctrls.append('print')

    def disable_task_controls(self) -> None:
        self.ctrls.remove('task')

    def input(self, data: Optional[Dict[str, Any]], online=False) -> None:
        if data is None:
            return

        if len(data) != 1:
            self.input_error('unexpected number of controls; expected: {}, got: {}'.format(1, len(data)))
            return
        control: str = next(iter(data))
        if control not in self.ctrls:
            self.input_error('unknown control: {}'.format(control))
            return

        self.controls_handler[control](data[control], online)

    def handle_task(self, data: str, online: bool) -> None:
        expected = ['check', 'reset']
        if data not in expected:
            self.input_error('task control: {}'.format(data))
            return

        if data == 'check':
            completed, reason = self.objective_fn()
            if completed:
                self.completed = True
            else:
                self.add_state(task={'completed': completed, 'reason': reason})
            return

        if data == 'reset':
            self.add_state(task={'completed': False, 'reason': 'task reset'})
            self.reset_state()
            return

    def handle_log(self, data: str, online: bool) -> None:
        expected = ['step', 'load', 'save']
        if data not in expected:
            self.input_error('log control: {}'.format(data))
            return

        assert self.log is not None

        if data == 'step':
            self.log.step(Execution(self.player))

        if data == 'load':
            if self.program is None:
                self.reset_state()
                self.log.from_file()
                self.log.step(Execution(self.player))

        if data == 'save':
            if online:
                self.input_error('online version: cannot save log')
                return
            self.log.to_file()

    def handle_player(self, data: str, online: bool) -> None:
        expected = ['move', 'turn_left', 'turn_right']
        if data not in expected:
            self.input_error('player control: {}'.format(data))
            return

        assert self.player is not None

        if data == 'move':
            self.player.move()
            if self.log is not None:
                self.log.append([Move()])

        if data == 'turn_left':
            self.player.turn_left()
            if self.log is not None:
                self.log.append([Turn('left')])

        if data == 'turn_right':
            self.player.turn_right()
            if self.log is not None:
                self.log.append([Turn('right')])

    def handle_program(self, data: Dict[str, str], online: bool) -> None:
        if 'action' not in data:
            self.input_error('program control: expect "action" field')
            return

        expected = ['step', 'back', 'update', 'load', 'save']
        action = data['action']
        if action not in expected:
            self.input_error('program control: {}'.format(action))
            return

        if action == 'update':
            if len(data) != 2:
                self.input_error('program control: expect 2 arguments, got {}'.format(len(data)))
                return
            if 'text' not in data:
                self.input_error('program control: expect "text" argument')
                return

            assert self.program is not None

            self.program.update(data['text'])
            self.reset_state()
            return

        if len(data) != 1:
            self.input_error('program control: expect 1 argument, got {}'.format(len(data)))
            return

        assert self.program is not None

        if action == 'step':
            self.log.append(self.program.step(self.execution))

        if action == 'back':
            self.log.remove(self.program.back(self.execution))

        if action == 'load':
            self.program.from_file()
            self.reset_state()

        if action == 'save':
            if online:
                self.input_error('online version: cannot save program')
                return
            self.program.to_file()

    def handle_map(self, data: Dict[str, str], online: bool) -> None:
        if len(data) != 2:
            self.input_error('map control: expect 2 arguments, got {}'.format(len(data)))
            return
        if 'action' not in data:
            self.input_error('map control: expect "action" field')
            return
        if 'name' not in data:
            self.input_error('map control: expect "name" field')
            return

        expected = ['choose', 'load', 'save']
        action = data['action']
        if action not in expected:
            self.input_error('map control: {}'.format(action))
            return

        if action == 'choose':
            if not self.map_load_path:
                self.board = Board.choose(data['name'])
                self.map_name = self.board.name
            else:
                self.map_name = data['name']
            self.reset_state()

        if action == 'load':
            raise NotImplementedError

        if action == 'save':
            if online:
                self.input_error('online version: cannot save map')
                return
            with open(os.path.join(self.map_save_path, self.map_name + '.lab'), 'w') as f:
                f.write(self.print_result)

    def render(self) -> Dict[str, Any]:
        result = {
            'name': self.name,
            'board': self.board.render(),
        }

        if self.error is not None:
            result['error'] = self.error
            self.error = None
            return result

        if self.completed:
            self.add_state(task={'completed': True})

        if self.log is not None:
            result['log'] = self.log.render()

        if self.player is not None:
            self.add_state(player=self.player.render())

        if self.program is not None:
            result['program'] = self.program.render()

        if self.state is not None:
            result['state'] = self.state
            self.state = None

        if self.print_result is not None:
            result['prettyPrint'] = self.print_result

        return result

    def check_goal(self) -> Tuple[bool, str]:
        pos = self.player.position
        for goal in self.board.goal_fields:
            if pos[0] == goal[0] and pos[1] == goal[1]:
                return True, ''

        return False, 'not on goal field'

    def timeout_fn(self, fn, *args):
        def handler():
            raise TimeoutError('timeout')

        signal.signal(signal.SIGALRM, handler)
        signal.alarm(1)

        result = None
        try:
            result = fn(*args)
        except TimeoutError:
            self.reset_state(fail=True)
        finally:
            signal.alarm(0)

        return result
