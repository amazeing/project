from server.model.player import Player


class Execution:
    player: Player

    def __init__(self, player: Player, program=None, child=False) -> None:
        self.player = player
        self.program = program
        self.trace = []
        self.branches = []
        self.turn_counter = [0]
        self.test_results = []
        self.player_states = []

        self.executed = []
        self.child = child

    def do(self):
        instr = self.program.instructions[self.program.line]
        self.trace.append(instr)
        instr.do(self)

        instrs = [instr]
        if self.executed:
            instrs += self.executed
            self.executed = []

        if self.player.test_goal():
            self.reset()
            self.program.line = len(self.program.instructions)

        return instrs

    def undo(self):
        instr = self.trace.pop()
        instr.undo(self)
        return instr

    def reset(self) -> None:
        self.trace = []
        self.branches = []
        self.turn_counter = [0]
        self.test_results = []
        self.player_states = []
        self.executed = []

    def remember_branch(self) -> None:
        self.branches.append(self.program.line)

    def forget_branch(self):
        return self.branches.pop()

    def turn_left(self):
        self.player.turn_left()
        self.turn_counter[-1] -= 1

    def turn_right(self):
        self.player.turn_right()
        self.turn_counter[-1] += 1

    def store_direction(self):
        self.turn_counter.append(0)

    def delete_direction(self):
        self.turn_counter.pop()

    def store_test(self, result):
        self.test_results.append(result)

    def delete_test(self):
        self.test_results.pop()

    def add_player_state(self, x, y, direction):
        self.player_states.append((x, y, direction,))

    def remove_player_state(self):
        return self.player_states.pop()
