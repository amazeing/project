from typing import Tuple, List, Dict, Any, Optional


class Board:
    name: str
    dimensions: Tuple[int, int]
    walls: List[List[bool]]
    start_fields: List[Tuple[int, int]]
    goal_fields: List[Tuple[int, int]]
    item_fields: List[Tuple[int, int]]
    properties: List[str]

    def __init__(self, name: str, dimensions: Tuple[int, int], walls: List[List[bool]],
                 start_fields: List[Tuple[int, int]], goal_fields: List[Tuple[int, int]],
                 item_fields: List[Tuple[int, int]], properties: List[str]) -> None:
        self.name = name
        self.dimensions = dimensions
        self.walls = walls
        self.start_fields = start_fields
        self.goal_fields = goal_fields
        self.item_fields = item_fields
        self.properties = properties

    def render(self) -> Dict[str, Any]:
        result = {
            'name': self.name,
            'dimensions': [x for x in self.dimensions],
            'walls': self.walls,
            'start_fields': [[x for x in s] for s in self.start_fields],
            'goal_fields': [[x for x in g] for g in self.goal_fields],
            'item_fields': [[x for x in i] for i in self.item_fields],
        }

        return result

    @staticmethod
    def choose(name: str) -> 'Board':
        return all_boards[name]

    @staticmethod
    def boards() -> List[str]:
        return list(all_boards.keys())

    def is_wall(self, field: Tuple[int, int]):
        return self.walls[field[0]][field[1]]


def text_to_board(name: str, text: str) -> Board:
    height: int = 0
    width: Optional[int] = None
    walls: List[List[bool]] = []
    start_fields: List[Tuple[int, int]] = []
    goal_fields: List[Tuple[int, int]] = []
    item_fields: List[Tuple[int, int]] = []

    for line in text.splitlines():
        if len(line) == 0:
            continue
        height += 1
        if width is None:
            width = len(line)
        assert width == len(line)

        row: List[bool] = []
        for i, c in enumerate(line.upper()):
            if c is 'X':
                row.append(True)
            elif c is '_':
                row.append(False)
            elif c is 'S':
                start_fields.append((i, height - 1))
                row.append(False)
            elif c is 'G':
                goal_fields.append((i, height - 1))
                row.append(False)
            elif c is 'I':
                item_fields.append((i, height - 1))
                row.append(False)
            else:
                assert False, "%c" % c
        walls.append(row)

    return Board(name, (width, height), walls, start_fields, goal_fields, item_fields, [])


empty = Board('Empty',
              (3, 4),
              [[True, True, True],
               [True, False, True],
               [True, False, True],
               [True, True, True]],
              [(1, 1)],
              [(1, 2)],
              [],
              [])

straight_text = '''
XXX
XGX
X_X
X_X
X_X
X_X
X_X
XSX
XXX
'''

straight_early_goal_text = '''
XXX
X_X
X_X
X_X
XGX
X_X
X_X
XSX
XXX
'''

fork_text = '''
XXXXXXXXXXXXXXXXXXXX
XXXXXXXX__S__XXXXXXX
XXXXXXXX_X_X_XXXXXXX
XXXXXXXX_X_X_XXXXXXX
XXXXXXXX_X_X_XXXXXXX
XXXXXXXX_X_X_XXXXXXX
XXX______X_X_XXXXXXX
X___XX_X_X_X_XXXXXXX
XGXXX__X_X_X_XXXXXXX
XX____XX_X_X_XXXXXXX
XX_XXXXX_X_X_XXXXXXX
XXGXXXXX_X_X_XXXXXXX
XXXXXXXX_X_X_XXXXXXX
XXXXXXXX_X_X_XXXXXXX
XXXXXXXX_X_X_XXXXXXX
XXXXXXXX_X_X_XXXXXXX
XXXXXXXXGX_XGXXXXXXX
XXXXXXXXXX_XXXXXXXXX
XXXXXXXXXXGXXXXXxXXX
XXXXXXXXXXXXXXXXXXXX
'''

item_fun = '''
XXXXXXXXXXXXXXXXXXXX
XXXXIIIIIIGIIIIIXXXX
XXXXXXXXXXIXXXXXXXXX
XXXXXXXXXXIXXXXXXXXX
XXXXXXXXXXIXXXXXXXXX
XXXXXXXXXXIXXXXXXXXX
XXXXXXXXXXIXXXXXXXXX
XXXXXXXXXXIXXXXXXXXX
XXXXXXXXXXIXXXXXXXXX
XXXXXXXXXXIXXXXXXXXX
XXXXXXXXXXIXXXXXXXXX
XXXXXXXXXXIXXXXXXXXX
XXXXXXXXXXIXXXXXXXXX
XXXXXXXXXXIXXXXXXXXX
XXXXIIIIIISIIIIIXXXX
XXXXXXXXXXXXXXXXXXXX
'''

gutter_text = '''
XXXXXXXXXXXXXXXXXXXX
X___XXXX_______XXXXX
X_X_XXXX_X_X_X_XXXXX
X_X_XXXX_X_X_X_XXXXX
X_X_XXXX_X_X_X_XXXXX
X_X_XXXX_X_X_X_XXXXX
X_X_XXXX_X_X_X_XXXXX
X_X_XXXX_X_X_X_XXXXX
X_X_XXXX_X_X_X_XXXXX
X_X_____SX_X_X____GX
X_XXXXXX_X_X_X_XXXXX
X_XXXXXX_X_X_X_XXXXX
X_XXXXXX_X_X_X_XXXXX
XGXXXXXX_X_X_X_XXXXX
XXXXXXXX_X_X_X_XXXXX
XXXXXXXX_X_X_X_XXXXX
XXXXXXXX_X_X_X_XXXXX
XXXXXXXX_______XXXXX
XXXXXXXXXXXXXXXXXXXX
XXXXXXXXXXXXXXXXXXXX
'''

dead_end_text = '''
XXXXXXXXXXXXXXXXXXXX
XXXXXXXX__S__XXXXXXX
X_X___XXXX_XXXX_XXXX
X_XXX____X___XX_XXXX
X_X___XX_X_X______XX
X_XXXXX__X_XX_XXXXXX
X_X_____XX_X__XXXXXX
X___XX_XXX_XX___XXXX
XXXXX_______XXXXXXXX
X_____XXXXXXXXXXX_XX
XX_XXXX______XXXX_XX
XX_XX___XXX_XXX____X
X___XXXXXX____XX_XXX
XXX______XXX_XX____X
X_X_X_XX________XXXX
X_XXX__XXXXX_XXXX__X
X_X___XX___X______XX
X___X__XXX_X_X_XXXXX
XX_XXX_X_____X____GX
XXXXXXXXXXXXXXXXXXXX
'''

binary_tree_text = '''
XXXXXXXXXXXXXXXXXXXX
XXXXXXXXXSXXXXXXXXXX
XXXXXXXXX_XXXXXXXXXX
XXX______________XXX
XXX_XXXXX_XXXXXX_XXX
X_____XXX_XXXX_____X
X_XXX_XXX_XXXX_XXX_X
X_XX___XX_XXX___XX_X
X_XX_X_XX_XXX_X_XX_X
XXXX_X_XX_XXX_X_XXXX
XXXXXXXXX_XXXXXXXXXX
XXXXXXXXX_XXXXXXXXXX
XX___XXXX_XXXXX___XX
XXXX____________XXXX
XX___XX_XXX_XXX___GX
XXXXXXX_XXX_XXXXXXXX
XXXXXX___X___XXXXXXX
XXXXXX_X_X_X_XXXXXXX
XXXXXX_X_X_X_XXXxXXX
XXXXXXXXXXXXXXXXXXXX
'''

binary_tree_short_text = '''
XXXXXXXXXXXXXXXXXXXX
XXXXXXXXGSXXXXXXXXXX
XXXXXXXXX_XXXXXXXXXX
XXX______________XXX
XXX_XXXXX_XXXXXX_XXX
X_____XXX_XXXX_____X
X_XXX_XXX_XXXX_XXX_X
X_XX___XX_XXX___XX_X
X_XX_X_XX_XXX_X_XX_X
XXXX_X_XX_XXX_X_XXXX
XXXXXXXXX_XXXXXXXXXX
XXXXXXXXX_XXXXXXXXXX
XX___XXXX_XXXXX___XX
XXXX____________XXXX
XX___XX_XXX_XXX___XX
XXXXXXX_XXX_XXXXXXXX
XXXXXX___X___XXXXXXX
XXXXXX_X_X_X_XXXXXXX
XXXXXX_X_X_X_XXXxXXX
XXXXXXXXXXXXXXXXXXXX
'''

choose_your_path_text = '''
XXXXXXXXXXXXXXXXXXXX
XXXXXXXX__S__XXXXXXX
XXXXXXXX_X_X_XXXXXXX
XXXXXXXX_X_X_XXXXXXX
XXXXXXXX_X_X_XXXXXXX
XXXXXXXX_X_X_XXXXXXX
XXXXXXXX_X_X_XXXXXXX
XXXXXXXX_X_X_XXXXXXX
XXXXXXXX_X_X_XXXXXXX
XXXXXXXX_X_X_XXXXXXX
XXXXXXXX_X_X_XXXXXXX
XXXXXXXX_X_X_XXXXXXX
XXXXXXXX_X_X_XXXXXXX
XXXXXXXX_X_X_XXXXXXX
XXXXXXXX_X_X_XXXXXXX
XXXXXXXX_X_X_XXXXXXX
XXXXXXXXGX_XGXXXXXXX
XXXXXXXXXX_XXXXXXXXX
XXXXXXXXXXGXXXXXXXXX
XXXXXXXXXXXXXXXXXXXX
'''

on_the_right_path_text = '''
XXXXXXXXXXXXXXXXXXXX
XXXXXXXX__S__XXXXXXX
XXXXXXXX_X_X_XXXXXXX
XXXXXXXX_X_X_XXXXXXX
XXXXXXXX_X_X_XXXXXXX
XXXXXXXX_X_X_XXXXXXX
XXXXXXXX_X_X_XXXXXXX
XXXXXXXX_X_X_XXXXXXX
XXXXXXXX_X_X_XXXXXXX
XXXXXXXX_X_X_XXXXXXX
XXXXXXXX_X_X_XXXXXXX
XXXXXXXX_X_X_XXXXXXX
XXXXXXXX_X_X_XXXXXXX
XXXXXXXX_X_X_XXXXXXX
XXXXXXXX_X_X_XXXXXXX
XXXXXXXX_X_X_XXXXXXX
XXXXXXXX_X_X_XXXXXXX
XXXXXXXXXX_XXXXXXXXX
XXXXXXXXXXGXXXXXXXXX
XXXXXXXXXXXXXXXXXXXX
'''

default_text = '''
XXXXXXXXXX
X____X___X
XG___X___X
X__X_X_X_X
XG_X_X_XSX
X__X___XSX
XXXXXXXXXX
'''

left_stuck_text = '''
XXXXXXXXXXXXXXXXXXXX
X__________________X
X_XXXXXXXXXXXXXXXX_X
X_XXXXXXXXXXXXXXXX_X
X_XXXXGXXXXXXXXXXX_X
X_XXXX________SXXX_X
X_XXXX_XXXXXXXXXXX_X
X_XXXX_XXXXXXXXXXX_X
X_XXXX_XXXXXXXXXXX_X
X_XXxX_XXXXXXXXXXX_X
X_XXXX_XXXXXXXXXXX_X
X_XXXX_XXXXXXXXXXX_X
X_XXXX_XXXXXXXXXXX_X
X_XXXX_XXXXXXXXXXX_X
X_XXXX_XXXXXXXXXXX_X
X____X_XXXXXXXXXXX_X
X_XX_X_XXXXXXXXXXX_X
X_XX_X_XXXXXXXXXXX_X
X____X_____________X
XXXXXXXXXXXXXXXXXXXX
'''

single_path_text = '''
XXXXXXXXXX
XXX___GXXX
XX__XXXXXX
XX_XXXXXXX
XX_XXXXXXX
XX_XXXXXXX
XX____XXXX
XXXX____XX
XXXXXXX_XX
XXXXXXX_XX
XXXXXXX_XX
XXXXXX__XX
XXS____XXX
XXXXXXXXXX
'''

right_round_text = '''
XXXXXXXXXXXXXXXXXXXX
XXXXXXXXXXXXXXXXXXXX
XXXXXXXXXXXXXXXXXXXX
XXXXXXXXXXXXXXXXXXXX
XXXXXXXXXXXXXXXXXXXX
XXXXXXXXXXXXXXXXXXXX
XXXXXXXXXXXXXXXXXXXX
XXXXXXXXXXXXXXXXXXXX
XXXX__________XXXXXX
XXXX_XXSXXGXX_XXXXXX
XXXX_XXXXXXXX_XXXXXX
XXXX__________XXXXXX
XXXXXXXXXXXXXXXXXXXX
XXXXXXXXXXXXXXXXXXXX
XXXXXXXXXXXXXXXXXXXX
XXXXXXXXXXXXXXXXXXXX
XXXXXXXXXXXXXXXXXXXX
XXXXXXXXXXXXXXXXXXXX
XXXXXXXXXXXXXXXXxXXX
XXXXXXXXXXXXXXXXXXXX
'''

right_turner_text = '''
XXXXXXXXXXXXXXXXXXXX
XXXXXXXXXXXXXXXXXXXX
XXXXXXXXXXXXXXXXXXXX
XXXXXXXXXXXXXXXXXXXX
XXXXXXXXXXXXXXXXXXXX
XXXXX___X___XXXXXXXX
XXXXX_X___X_XXXXXXXX
XXXXX_XGXSX_XXXXXXXX
XXXXX_XXXXX_XXXXXXXX
XXXXX_______XXXXXXXX
XXXXXXXXXXXXXXXXXXXX
XXXXXXXXXXXXXXXXXXXX
XXXXXXXXXXXXXXXXXXXX
XXXXXXXXXXXXXXXXXXXX
XXXXXXXXXXXXXXXXXXXX
XXXXXXXXXXXXXXXXXXXX
XXXXXXXXXXXXXXXXXXXX
XXXXXXXXXXXXXXXXXXXX
XXXXXXXXXXXXXXXXxXXX
XXXXXXXXXXXXXXXXXXXX
'''

nearly_straight_text = '''
XXXXXXXXXXXXXXXXXXXX
XXXXXXXXXXXXXXXXXXXX
XXXXXXXXXXXXXXXXXXXX
XXXXXXXXXXXXXXXXXXXX
XXXXXXXXXXXXXXXXXXXX
XXXXXXGXXXXXXXXXXXXX
XXXX______XXXXXXXXXX
XXXX_XXSX_XXXXXXXXXX
XXXX_XXXX_XXXXXXXXXX
XXXX_XXXX_XXXXXXXXXX
XXXX______XXXXXXXXXX
XXXXXXXXXXXXXXXXXXXX
XXXXXXXXXXXXXXXXXXXX
XXXXXXXXXXXXXXXXXXXX
XXXXXXXXXXXXXXXXXXXX
XXXXXXXXXXXXXXXXXXXX
XXXXXXXXXXXXXXXXXXXX
XXXXXXXXXXXXXXXXXXXX
XXXXXXXXXXXXXXXXxXXX
XXXXXXXXXXXXXXXXXXXX
'''

the_hammer_text = '''
XXXXXXXXXXXXXXXXXXXX
XXXXXXXXXXXXXXXXXXXX
XXXXXXXXXXXXXXXXXXXX
XXXXXXXXXXXXXXXXXXXX
XXXXXXXXXXXXXXXXXXXX
XXXXXXXXXXXXXXXXXXXX
XXXXXXXXXXXXXXXXXXXX
XXXXX_________XXXXXX
XXXXX_X_X_X_X_XXXXXX
XXXXX___X_X___XXXXXX
XXXXXXXXX_XXXXXXXXXX
XXXXXXXXXSXXXXXXXXXX
XXXXXXXXX_XXXXXXXXXX
XXXXXXXXXGXXXXXXXXXX
XXXXXXXXXXXXXXXXXXXX
XXXXXXXXXXXXXXXXXXXX
XXXXXXXXXXXXXXXXXXXX
XXXXXXXXXXXXXXXXXXXX
XXXXXXXXXXXXXXXXxXXX
XXXXXXXXXXXXXXXXXXXX
'''

right_stuck_text = '''
XXXXXXXXXXXXXXXXXXXX
XX___XXXXXXXXXXXXXXX
XX_X_XXXXXXXXXXXXXXX
XX___XXXXXXXXXXXXXXX
XX_XXXXXXXXXXXXXXXXX
XX_______________SXX
XXGXXXXXXXXXXXXXXXXX
XXXXXXXXXXXXXXXXXXXX
XXXXXXXXXXXXXXXXXXXX
XXXXxXXXXXXXXXXXXXXX
XXXXXXXXXXXXXXXXXXXX
XXXXXXXXXXXXXXXXXXXX
XXXXXXXXXXXXXXXXXXXX
XXXXXXXXXXXXXXXXXXXX
XXXXXXXXXXXXXXXXXXXX
XXXXXXXXXXXXXXXXXXXX
XXXXXXXXXXXXXXXXXXXX
XXXXXXXXXXXXXXXXXXXX
XXXXXXXXXXXXXXXXxXXX
XXXXXXXXXXXXXXXXXXXX
'''

all_boards = {
    'Empty': empty,
    'Straight': text_to_board('Straight', straight_text),
    'Straight early goal': text_to_board('Straight early goal', straight_early_goal_text),
    'Fork': text_to_board('Fork', fork_text),
    'Gutter': text_to_board('Gutter', gutter_text),
    'Dead-end': text_to_board('Dead-end', dead_end_text),
    'Binary tree': text_to_board('Binary tree', binary_tree_text),
    'Binary tree (short)': text_to_board('Binary tree (short)', binary_tree_short_text),
    'Choose your path': text_to_board('Choose your path', choose_your_path_text),
    'On the right path': text_to_board('On the right path', on_the_right_path_text),
    'Default': text_to_board('Default', default_text),
    'Left stuck': text_to_board('Left stuck', left_stuck_text),
    'Single path': text_to_board('Single path', single_path_text),
    'Right round': text_to_board('Right round', right_round_text),
    'Right turner': text_to_board('Right turner', right_turner_text),
    'Nearly straight': text_to_board('Nearly straight', nearly_straight_text),
    'The hammer!': text_to_board('The hammer!', the_hammer_text),
    'Right stuck': text_to_board('Right stuck', right_stuck_text),
    'ITEM FUN :3': text_to_board('ITEM FUN :3', item_fun),
}
