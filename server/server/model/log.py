import os
from typing import Union, Dict, Any, List

from server.model.execution import Execution
from server.model.instructions import Instruction, Move, Turn
from server.util.instruction_parser import InstructionParser


class Log:
    name: str
    file_path: Union[bytes, str]
    instructions: List[Instruction]
    parser: InstructionParser
    directory: Union[bytes, str] = os.path.realpath(os.path.join(os.path.dirname(__file__), '../impl/logs'))
    stepping: bool
    line: int

    def __init__(self, name: str) -> None:
        if not os.path.exists(self.directory):
            os.makedirs(self.directory)

        self.name = name
        self.file_path = os.path.join(Log.directory, self.name + '.log')
        self.instructions = []
        self.parser = InstructionParser('move', 'turn')
        self.stepping = False
        self.line = 0

    def parse(self, text: str) -> None:
        self.instructions = self.parser.parse(text)
        self.stepping = False
        self.line = 0

    def dump(self) -> str:
        if not self.stepping:
            return '\n'.join(str(inst) for inst in self.instructions)
        else:
            return '\n'.join(str(inst) for inst in self.instructions[:self.line + 1])

    def step(self, execution: Execution) -> None:
        if self.stepping and self.line < len(self.instructions):
            self.instructions[self.line].do(execution)
            self.line += 1
            if self.line == len(self.instructions):
                self.stepping = False

    def render(self) -> Dict[str, Any]:
        result = {
            'text': self.dump(),
        }

        if self.stepping and self.line < len(self.instructions):
            result['continues'] = True

        return result

    def from_file(self) -> None:
        if not os.path.isfile(self.file_path):
            self.parse('')
            return

        with open(self.file_path) as logfile:
            self.parse(logfile.read())

        self.stepping = True

    def to_file(self) -> None:
        with open(self.file_path, 'w') as logfile:
            logfile.write(self.dump())

    def append(self, instructions: List[Instruction]) -> None:
        for instruction in instructions:
            if isinstance(instruction, Move) or isinstance(instruction, Turn):
                self.instructions.append(instruction)

    def remove(self, instructions: List[Instruction]) -> None:
        for instruction in instructions:
            if isinstance(instruction, Move) or isinstance(instruction, Turn):
                self.instructions.remove(instruction)
