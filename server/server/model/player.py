from typing import Tuple, Dict, Any, Optional

from server.model.board import Board


class Player:
    board: Board
    position: Tuple[int, int]
    direction: int
    radius: Optional[int]

    directions = [
        ((0, -1), 'north'),
        ((1, 0), 'east'),
        ((0, 1), 'south'),
        ((-1, 0), 'west'),
    ]

    def __init__(self, board: Board, start: int = 0, radius: Optional[int] = None) -> None:
        self.board = board
        self.position = self.board.start_fields[start]
        self.direction = 0
        self.radius = radius

    def reset(self, board: Board, start: int = 0):
        self.board = board
        self.position = self.board.start_fields[start]
        self.direction = 0

    def render(self) -> Dict[str, Any]:
        result = {
            'position': [x for x in self.position],
            'direction': self.directions[self.direction][1],
        }

        if self.radius is not None:
            result['radius'] = self.radius

        return result

    def move(self):
        offset = self.directions[self.direction][0]
        x = self.position[0] + offset[0]
        y = self.position[1] + offset[1]
        if not self.board.walls[y][x]:
            self.position = (x, y)

    def turn_left(self):
        self.direction -= 1
        self.direction %= len(self.directions)

    def turn_right(self):
        self.direction += 1
        self.direction %= len(self.directions)

    def test_goal(self):
        for goal in self.board.goal_fields:
            if self.position[0] == goal[0] and self.position[1] == goal[1]:
                return True

        return False

    def is_wall_in_direction(self, direction: str) -> bool:
        if direction == 'front':
            offset = self.directions[self.direction]
        elif direction == 'back':
            offset = self.directions[(self.direction + 2) % 4]
        elif direction == 'left':
            offset = self.directions[(self.direction - 1) % 4]
        elif direction == 'right':
            offset = self.directions[(self.direction + 1) % 4]
        else:
            return False

        x_off, y_off = offset[0][0], offset[0][1]
        return self.board.walls[self.position[1] + y_off][self.position[0] + x_off]
