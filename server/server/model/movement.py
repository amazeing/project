from server.model.instructions import Move, Turn

calls = []


def move():
    calls.append(Move())


def turn_right():
    calls.append(Turn('right'))


def turn_left():
    calls.append(Turn('left'))
