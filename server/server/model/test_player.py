from unittest import TestCase

from server.model.board import Board
from server.model.player import Player


class TestPlayer(TestCase):
    def setUp(self):
        board = Board('', (3, 2), [[True, True], [False, False], [False, True]], [(0, 2)], [], [], [])
        self.player = Player(board)

    def test_move(self):
        self.player.move()
        self.assertEqual((0, 1), self.player.position)
        self.player.move()
        self.assertEqual((0, 1), self.player.position)
        self.player.turn_right()
        self.player.move()
        self.assertEqual((1, 1), self.player.position)

    def test_turn_left(self):
        self.assertEqual(0, self.player.direction)
        self.player.turn_left()
        self.assertEqual(3, self.player.direction)
        self.player.turn_left()
        self.assertEqual(2, self.player.direction)
        self.player.turn_left()
        self.assertEqual(1, self.player.direction)
        self.player.turn_left()
        self.assertEqual(0, self.player.direction)
        self.player.turn_left()

    def test_turn_right(self):
        self.assertEqual(0, self.player.direction)
        self.player.turn_right()
        self.assertEqual(1, self.player.direction)
        self.player.turn_right()
        self.assertEqual(2, self.player.direction)
        self.player.turn_right()
        self.assertEqual(3, self.player.direction)
        self.player.turn_right()
        self.assertEqual(0, self.player.direction)
        self.player.turn_right()

    def test_render(self):
        result = self.player.render()
        self.assertIn('position', result)
        self.assertIn('direction', result)
        self.assertNotIn('radius', result)

        self.assertListEqual([0, 2], result['position'])
        self.assertEqual('north', result['direction'])

        self.player.move()
        result = self.player.render()
        self.assertListEqual([0, 1], result['position'])
        self.assertEqual('north', result['direction'])

        self.player.turn_right()
        result = self.player.render()
        self.assertListEqual([0, 1], result['position'])
        self.assertEqual('east', result['direction'])

    def test_render_radius(self):
        self.player.radius = 1
        result = self.player.render()
        self.assertIn('position', result)
        self.assertIn('direction', result)
        self.assertIn('radius', result)
        self.assertEqual(1, result['radius'])
