from server.model.instructions import Move, Turn
from server.model.player import Player

execution = None
calls = []


def setup(exe):
    global execution
    execution = exe


def move():
    calls.append(Move())
    execution.player.move()


def turn_right():
    calls.append(Turn('right'))
    execution.player.turn_right()


def turn_left():
    calls.append(Turn('left'))
    execution.player.turn_left()


def get_player_position():
    return execution.player.position


def get_player_direction():
    pos, name = Player.directions[execution.player.direction]
    return pos[0], pos[1], name


def test_goal():
    pos = execution.player.position
    for goal in execution.player.board.goal_fields:
        if pos[0] == goal[0] and pos[1] == goal[1]:
            return True

    return False


def test_wall(x, y):
    if x < 0 or x >= execution.player.board.dimensions[0] or y < 0 or y >= execution.player.board.dimensions[1]:
        raise ValueError

    return execution.player.board.walls[y][x]
