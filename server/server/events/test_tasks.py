from unittest import TestCase
from unittest.mock import MagicMock

from server.config import app, sio
from server.events import InputError
from server.events.tasks import Tasks


class TestTasks(TestCase):
    def setUp(self):
        self.tasks = Tasks(sio)

        self.controls = ['test_control_1', 'test_control_2', 'test_control_3']
        self.render_data = {'component': 'options'}
        self.input_data = {'input': 'options'}

        self.mock_task = MagicMock()
        self.mock_task.controls = MagicMock(return_value=self.controls)
        self.mock_task.render.return_value = self.render_data
        self.mock_task_class = MagicMock(return_value=self.mock_task)
        self.tasks.handler['test'] = self.mock_task_class

        self.client = sio.test_client(app, namespace=Tasks.namespace)
        self.assertTrue(self.client.is_connected(namespace=Tasks.namespace))

        self.assertIn(self.client.sid, self.tasks.running)
        self.assertIsNone(self.tasks.running[self.client.sid])

    def tearDown(self):
        self.client.disconnect(namespace=Tasks.namespace)
        self.assertNotIn(self.client.sid, self.tasks.running)

    def emit_start(self, task):
        self.client.emit('start', task, namespace=Tasks.namespace)

    def emit_input(self, data):
        self.client.emit('input', data, namespace=Tasks.namespace)

    def assert_received(self, events):
        received = self.client.get_received(namespace=Tasks.namespace)
        self.assertEqual(len(events), len(received))

        result = []
        for (e, r) in zip(events, received):
            self.assertEqual(e, r['name'])
            result.append(r['args'])

        return result

    def test_input_before_start(self):
        self.emit_input({})
        self.assert_received(['error'])

    def test_start(self):
        self.emit_start('test')
        self.assertIsInstance(self.tasks.running[self.client.sid], MagicMock)
        self.mock_task_class.assert_called_once_with()

        received = self.client.get_received(namespace=Tasks.namespace)
        self.assertEqual(2, len(received))

    def test_controls(self):
        self.emit_start('test')
        received = self.assert_received(['controls', 'render'])

        controls_args = received[0]
        self.assertEqual(1, len(controls_args))
        self.assertListEqual(self.controls, controls_args[0])

    def test_render(self):
        self.emit_start('test')
        received = self.assert_received(['controls', 'render'])

        render_args = received[1]
        self.assertEqual(1, len(render_args))
        self.assertDictEqual(self.render_data, render_args[0])
        self.mock_task.input.assert_called_once_with(None, False)
        self.mock_task.render.assert_called_once_with()

    def test_input(self):
        self.emit_start('test')
        self.assert_received(['controls', 'render'])
        self.mock_task.reset_mock()

        self.emit_input(self.input_data)
        received = self.assert_received(['render'])

        render_args = received[0]
        self.assertEqual(1, len(render_args))
        self.assertDictEqual(self.render_data, render_args[0])
        self.mock_task.input.assert_called_once_with(self.input_data, False)
        self.mock_task.render.assert_called_once_with()

    def test_error(self):
        self.mock_task.input.side_effect = InputError('test message')
        self.emit_start('test')
        received = self.assert_received(['controls', 'error'])

        error_args = received[1]
        self.assertEqual(1, len(error_args))
        self.assertEqual('test message', error_args[0]['message'])

    def test_invalid_day_task(self):
        tests = [
            '0_1',
            '5_1',
            '1_4',
        ]

        for test in tests:
            self.emit_start(test)
            self.assert_received(['error'])

    def test_valid_day_task(self):
        tests = [
            '1_1_1',
            '1_2_1',
            '1_3_1',
        ]

        for test in tests:
            self.emit_start(test)
            self.assert_received(['controls', 'render'])
