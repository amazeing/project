class InputError(Exception):
    def __init__(self, message: str) -> None:
        self.msg = message
