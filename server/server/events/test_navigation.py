from unittest import TestCase

from server.config import app, sio
from server.events.navigation import Navigation


class TestNavigation(TestCase):
    def setUp(self):
        self.nav = Navigation(sio)

        self.client = sio.test_client(app, namespace=Navigation.namespace)
        self.assertTrue(self.client.is_connected(namespace=Navigation.namespace))

    def assert_received(self):
        received = self.client.get_received(namespace=Navigation.namespace)
        self.assertEqual(1, len(received))
        self.assertEqual('update', received[0]['name'])

        menu = received[0]['args'][0]
        return self.assert_menu('root', 'aMazeing Project', menu)

    def assert_menu(self, name, title, menu):
        self.assertEqual(name, menu['id'])
        self.assertEqual(title, menu['title'])

        return menu['buttons']

    def assert_button(self, name, text, buttons):
        for b in buttons:
            if b['name'] == name:
                self.assertEqual(text, b['text'])
                return b

        self.fail()

    def test_initial_update(self):
        received = self.client.get_received(namespace=Navigation.namespace)
        self.assertEqual(1, len(received))
        self.assertEqual('update', received[0]['name'])

    def test_play(self):
        play_button = self.assert_button('play', 'Play', self.assert_received())
        self.assertEqual('menu', play_button['action']['kind'])
        buttons = self.assert_menu('play', 'Play', play_button['action']['event'])

        self.assert_button('single-player', 'Single-player', buttons)
        self.assert_button('multi-player', 'Multi-player', buttons)
        self.assert_button('tournament', 'Tournament', buttons)

    def test_tasks(self):
        tasks_button = self.assert_button('tasks', 'Tasks', self.assert_received())
        self.assertEqual('menu', tasks_button['action']['kind'])
        tasks_buttons = self.assert_menu('tasks_menu', 'Tasks', tasks_button['action']['event'])

        day1_button = self.assert_button('day1', 'Day 1 - Introduction', tasks_buttons)
        self.assertEqual('menu', day1_button['action']['kind'])

    def test_credits(self):
        credits_button = self.assert_button('credits', 'Credits', self.assert_received())
        self.assertEqual('page', credits_button['action']['kind'])
        self.assertIn('event', credits_button['action'])
