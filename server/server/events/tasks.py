from typing import Dict, Any, Optional, Type

import flask_socketio
from flask import request
from flask_socketio import SocketIO

from server import tasks
from server.events import InputError
from server.model.task import Task


class Tasks(flask_socketio.Namespace):
    socket: SocketIO
    running: Dict[int, Optional[Task]]
    handler: Dict[str, Type[Task]]
    namespace: str = '/tasks'

    def __init__(self, socket: flask_socketio.SocketIO) -> None:
        super().__init__(namespace=self.namespace)

        self.socket = socket
        self.socket.on_namespace(self)

        self.online = False

        self.running = {}
        self.handler = {
            '1_0': tasks.task_1_0.Task,
            '1_1_1': tasks.task_1_1_1.Task,
            '1_1_2': tasks.task_1_1_2.Task,
            '1_2_1': tasks.task_1_2_1.Task,
            '1_2_2': tasks.task_1_2_2.Task,
            '1_3_1': tasks.task_1_3_1.Task,
            '1_3_2': tasks.task_1_3_2.Task,
            '2_1_1': tasks.task_2_1_1.Task,
            '2_1_2': tasks.task_2_1_2.Task,
            '2_2_1': tasks.task_2_2_1.Task,
            '2_2_2': tasks.task_2_2_2.Task,
            '2_3_1': tasks.task_2_3_1.Task,
            '2_3_2': tasks.task_2_3_2.Task,
            '2_4_1': tasks.task_2_4_1.Task,
            '2_4_2': tasks.task_2_4_2.Task,
            '2_5_1': tasks.task_2_5_1.Task,
            '2_5_2': tasks.task_2_5_2.Task,
            '2_6_1': tasks.task_2_6_1.Task,
            '2_6_2': tasks.task_2_6_2.Task,
            '3_1': tasks.task_3_1.Task,
            '3_2': tasks.task_3_2.Task,
            '3_3': tasks.task_3_3.Task
        }

    def set_online(self):
        self.online = True

    def on_connect(self) -> None:
        self.running[request.sid] = None

    def on_disconnect(self) -> None:
        del self.running[request.sid]

    def on_start(self, task: str) -> None:
        if task not in self.handler:
            self.emit('error', {'message': 'task does not exist'})
            return

        task = self.handler[task]()
        self.running[request.sid] = task
        self.emit('controls', task.controls())
        self.on_input(None)

    def on_input(self, data: Any) -> None:
        if self.running[request.sid] is None:
            self.emit('error', {'message': 'task must be started first'})
            return

        task = self.running[request.sid]
        try:
            task.input(data, self.online)
        except InputError as e:
            self.emit('error', {'message': e.msg})
            return

        self.emit('render', task.render())
