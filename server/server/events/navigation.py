from typing import Any, Dict, Union

import flask_socketio
from flask_socketio import SocketIO


class Navigation(flask_socketio.Namespace):
    socket: SocketIO
    namespace: str = '/nav'

    def __init__(self, socket: flask_socketio.SocketIO) -> None:
        super().__init__(namespace=self.namespace)

        self.socket = socket
        self.socket.on_namespace(self)

    def on_connect(self) -> None:
        self.update(broadcast=False)

    def update(self, broadcast: bool = True) -> None:
        nav = menu(
            'root',
            'aMazeing Project',
            self.play(),
            self.tasks(),
            self.credits(),
        )

        event = 'update'
        if broadcast:
            self.socket.emit(event, nav, namespace=self.namespace)
        else:
            self.emit(event, nav, namespace=self.namespace)

    @staticmethod
    def play() -> Dict[str, Any]:
        play_menu = menu(
            'play',
            'Play',
            button('single-player', 'Single-player', button_action('page', '/todo')),
            button('multi-player', 'Multi-player', button_action('page', '/todo')),
            button('tournament', 'Tournament', button_action('page', '/todo')),
        )

        action = button_action('menu', play_menu)
        return button('play', 'Play', action)

    @staticmethod
    def tasks() -> Dict[str, Any]:
        day1_task1_menu = menu(
            'task_1_1_menu',
            'Task 1 - Clicks',
            button('task_1_1_1', 'Labyrinth 1', button_action('page', '/tasks/1_1_1')),
            button('task_1_1_2', 'Labyrinth 2', button_action('page', '/tasks/1_1_2')),
        )

        day1_task2_menu = menu(
            'task_1_2_menu',
            'Task 2 - Log',
            button('task_1_2_1', 'Labyrinth 1', button_action('page', '/tasks/1_2_1')),
            button('task_1_2_2', 'Labyrinth 2', button_action('page', '/tasks/1_2_2')),
        )

        day1_task3_menu = menu(
            'task_1_3_menu',
            'Task 3 - Python',
            button('task_1_3_1', 'Labyrinth 1', button_action('page', '/tasks/1_3_1')),
            button('task_1_3_2', 'Labyrinth 2', button_action('page', '/tasks/1_3_2')),
        )

        day1_menu = menu(
            'day1_menu',
            'Day 1 - Introduction',
            button('task_1_0', 'Task 0 - Introduction', button_action('page', '/tasks/1_0')),
            button('task_1_1', 'Task 1 - Click', button_action('menu', day1_task1_menu)),
            button('task_1_2', 'Task 2 - Log', button_action('menu', day1_task2_menu)),
            button('task_1_3', 'Task 3 - Python', button_action('menu', day1_task3_menu)),
        )

        day2_task1 = menu(
            'task_2_1_menu',
            'Task 1 - Move until wall',
            button('task_2_1_1', 'Instructions', button_action('page', '/tasks/2_1_1')),
            button('task_2_1_2', 'Python', button_action('page', '/tasks/2_1_2')),
        )

        day2_task2 = menu(
            'task_2_2_menu',
            'Task 2 - Right turner',
            button('task_2_2_1', 'Instructions', button_action('page', '/tasks/2_2_1')),
            button('task_2_2_2', 'Python', button_action('page', '/tasks/2_2_2')),
        )

        day2_task3 = menu(
            'task_2_3_menu',
            'Task 3 - Follow wall',
            button('task_2_3_1', 'Instructions', button_action('page', '/tasks/2_3_1')),
            button('task_2_3_2', 'Python', button_action('page', '/tasks/2_3_2')),
        )

        day2_task4 = menu(
            'task_2_4_menu',
            'Task 4 - Wall follower',
            button('task_2_4_1', 'Instructions', button_action('page', '/tasks/2_4_1')),
            button('task_2_4_2', 'Python', button_action('page', '/tasks/2_4_2')),
        )

        day2_task5 = menu(
            'task_2_5_menu',
            'Task 5 - Follow obstacle',
            button('task_2_5_1', 'Instructions', button_action('page', '/tasks/2_5_1')),
            button('task_2_5_2', 'Python', button_action('page', '/tasks/2_5_2')),
        )

        day2_task6 = menu(
            'task_2_6_menu',
            'Task 6 - Pledge',
            button('task_2_6_1', 'Instructions', button_action('page', '/tasks/2_6_1')),
            button('task_2_6_2', 'Python', button_action('page', '/tasks/2_6_2')),
        )

        day2_menu = menu(
            'day2_menu',
            'Day 2 - Algorithms',
            button('task_2_1', 'Task 1 - Move until wall', button_action('menu', day2_task1)),
            button('task_2_2', 'Task 2 - Right turner', button_action('menu', day2_task2)),
            button('task_2_3', 'Task 3 - Follow wall', button_action('menu', day2_task3)),
            button('task_2_4', 'Task 4 - Wall follower', button_action('menu', day2_task4)),
            button('task_2_5', 'Task 5 - Follow obstacle', button_action('menu', day2_task5)),
            button('task_2_6', 'Task 6 - Pledge', button_action('menu', day2_task6)),
        )

        day3_menu = menu(
            'day3_menu',
            'Day 3 - File I/O',
            button('task_3_1', 'Parse file', button_action('page', '/tasks/3_1')),
            button('task_3_2', 'Visualize map', button_action('page', '/tasks/3_2')),
            button('task_3_3', 'Print to file', button_action('page', '/tasks/3_3'))
        )

        tasks_menu = menu(
            'tasks_menu',
            'Tasks',
            button('day1', 'Day 1 - Introduction', button_action('menu', day1_menu)),
            button('day2', 'Day 2 - Algorithms', button_action('menu', day2_menu)),
            button('day3', 'Day 3 - File I/O', button_action('menu', day3_menu))
        )

        action = button_action('menu', tasks_menu)
        return button('tasks', 'Tasks', action)

    @staticmethod
    def credits() -> Dict[str, Any]:
        action = button_action('page', '/credits')
        return button('credits', 'Credits', action)


def menu(name: str, title: str, *buttons: Dict[str, Any]) -> Dict[str, Any]:
    return {
        'id': name,
        'title': title,
        'buttons': buttons,
    }


def button(name: str, text: str, action: Dict[str, Any]) -> Dict[str, Any]:
    return {
        'name': name,
        'text': text,
        'action': action,
    }


def button_action(kind: str, event: Union[str, Dict[str, Any]]) -> Dict[str, Any]:
    return {
        'kind': kind,
        'event': event,
    }
