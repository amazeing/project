from typing import Dict, Callable, Any, Tuple, List

from server.events import InputError
from server.model.instructions import Builder, Instruction


class InstructionParser:
    parsers: Dict[str, Callable[[Any, Tuple[str, ...]], None]]
    builder: Builder

    def __init__(self, *args: str) -> None:
        self.parsers = {
            'move': self.parse_move,
            'turn': self.parse_turn,
            'store': self.parse_store,
            'test': self.parse_test,
            'jump': self.parse_jump,
            'branch': self.parse_branch,
            'call': self.parse_call,
            'return': self.parse_return,
        }

        for p in [p for p in self.parsers if p not in args]:
            del self.parsers[p]

        self.builder = Builder()

    def parse(self, text: str) -> List[Instruction]:
        self.builder.reset()

        for line in text.splitlines():
            self.parse_line(line)

        return self.builder.finish()

    def parse_line(self, line: str) -> None:
        words: List[str] = line.split(' ')
        if len(words) == 1 and words[0] == '':
            self.builder.empty()
            return

        if len(words) == 1 and len(words[0]) > 1 and words[0][-1] == ':' and words[0][:-1].isidentifier():
            self.builder.label(words[0][:-1])
            return
        elif len(words) >= 1 and words[0][-1] == ':':
            raise InputError('invalid label')

        for word in words:
            if not word.isidentifier():
                raise InputError('invalid token')

        self.parse_command(words[0], *words[1:])

    def parse_command(self, name: str, *args: str) -> None:
        if name not in self.parsers:
            raise InputError('invalid command')

        self.parsers[name](*args)

    def parse_move(self, *args: str) -> None:
        if len(args) > 0:
            raise InputError('"move" does not expect arguments')

        self.builder.move()

    def parse_turn(self, *args: str) -> None:
        expected = ['left', 'right']
        if len(args) != 1 or args[0] not in expected:
            raise InputError('"turn" expects exactly one argument: ' +
                             ' or '.join(['"{}"'.format(arg) for arg in expected]))

        self.builder.turn(args[0])

    def parse_store(self, *args: str) -> None:
        expected = ['direction', 'mark', 'state']
        if len(args) != 1 or args[0] not in expected:
            raise InputError('"store" expects exactly one argument: ' +
                             ' or '.join(['"{}"'.format(arg) for arg in expected]))

        self.builder.store(args[0])

    def parse_test(self, *args: str) -> None:
        if len(args) > 2:
            raise InputError('"test" received to many arguments')

        expected = ['mark', 'state', 'goal', 'direction']
        if len(args) == 1 and args[0] not in expected:
            raise InputError('"test" expects exactly one argument: ' +
                             ' or '.join(['"{}"'.format(arg) for arg in expected + ['wall']]))

        expected_wall = ['front', 'back', 'left', 'right']
        if len(args) == 2 and (args[0] != 'wall' or args[1] not in expected_wall):
            raise InputError('"test wall" expects exactly one argument: ' +
                             ' or '.join(['"{}"'.format(arg) for arg in expected_wall]))

        self.builder.test(*args)

    def parse_jump(self, *args: str) -> None:
        if len(args) != 1:
            raise InputError('"jump" expects exactly one label as argument')

        self.builder.jump(args[0])

    def parse_branch(self, *args: str) -> None:
        if len(args) != 2:
            raise InputError('"branch" expects exactly two labels as arguments')

        self.builder.branch(args[0], args[1])

    def parse_call(self, *args: str) -> None:
        if len(args) != 1:
            raise InputError('"call" expects exactly one function as argument')

        self.builder.call(args[0])

    def parse_return(self, *args: str) -> None:
        if len(args) != 0:
            raise InputError('"return" expects no arguments')

        self.builder.return_instr()
