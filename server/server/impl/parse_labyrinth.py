from typing import Tuple, List

from server.model.board import Board


def parse_labyrinth(path: str):
    with open(path) as file:
        lines = file.read().splitlines()

    assert lines
    name = lines.pop(0)

    # skip empty lines
    while lines and not lines[0]:
        lines.pop(0)

    assert lines
    numbers = lines.pop(0).split()
    assert len(numbers) == 2
    dimensions = (int(numbers[0]), int(numbers[1]))

    # skip empty lines
    while lines and not lines[0]:
        lines.pop(0)

    start_fields: List[Tuple[int, int]] = []
    goal_fields: List[Tuple[int, int]] = []
    item_fields: List[Tuple[int, int]] = []
    walls: List[List[bool]] = []

    assert lines and 0 < dimensions[0] and 0 < dimensions[1] <= len(lines)
    for i in range(dimensions[1]):
        row: List[bool] = []

        assert len(lines[0]) == dimensions[0]
        for j, c in enumerate(lines.pop(0).upper()):
            assert c in ['_', 'X', 'S', 'G', 'I']

            if c is 'X':
                row.append(True)
            else:
                row.append(False)

                if c is 'S':
                    start_fields.append((j, i))
                elif c is 'G':
                    goal_fields.append((j, i))
                elif c is 'I':
                    item_fields.append((j, i))

        walls.append(row)

    assert start_fields and goal_fields

    # skip empty lines
    while lines and not lines[0]:
        lines.pop(0)

    # optional
    properties: List[str] = []
    while lines and lines[0]:
        properties.append(lines.pop(0))

    # all remaining lines have to be empty
    for l in lines:
        assert not l

    return Board(name, dimensions, walls, start_fields, goal_fields, item_fields, properties)
