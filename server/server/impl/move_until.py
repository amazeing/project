from server.model import algorithms


def move_until():
    if algorithms.test_goal():
        return

    x, y = algorithms.get_player_position()
    x_off, y_off, name = algorithms.get_player_direction()

    while not algorithms.test_wall(x + x_off, y + y_off):
        algorithms.move()
        x, y = algorithms.get_player_position()
        if algorithms.test_goal():
            return
