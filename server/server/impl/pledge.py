from server.impl.follow_obstacle import follow_obstacle
from server.impl.move_until import move_until
from server.model import algorithms


def pledge():
    move_until()
    while not algorithms.test_goal():
        follow_obstacle()
        move_until()
