from typing import List

from server.model.board import Board


def print_labyrinth(board: Board) -> str:
    lines: List[str] = [board.name, str(board.dimensions[0]) + ' ' + str(board.dimensions[1])]

    for i in range(board.dimensions[1]):
        row = ''

        for j in range(board.dimensions[0]):
            if (j, i) in board.start_fields:
                row += 'S'
            elif (j, i) in board.goal_fields:
                row += 'G'
            elif (j, i) in board.item_fields:
                row += 'I'
            elif board.walls[i][j]:
                row += 'X'
            else:
                row += '_'

        lines.append(row)

    lines += board.properties

    return '\n'.join(lines)
