from server.impl.follow_wall import follow_wall
from server.model import algorithms


def follow_obstacle():
    if algorithms.test_goal():
        return

    right_off = {
        'north': (1, 0),
        'east': (0, 1),
        'south': (-1, 0),
        'west': (0, -1),
    }

    algorithms.turn_left()
    turns = -1

    follow_wall()
    while turns != 0 and not algorithms.test_goal():
        x, y = algorithms.get_player_position()
        x_off, y_off, name = algorithms.get_player_direction()
        right_x_off = right_off[name][0]
        right_y_off = right_off[name][1]

        if not algorithms.test_wall(x + right_x_off, y + right_y_off):
            algorithms.turn_right()
            turns += 1
            algorithms.move()
        elif algorithms.test_wall(x + x_off, y + y_off):
            algorithms.turn_left()
            turns -= 1

        follow_wall()
