from server.model import algorithms


def follow_wall():
    if algorithms.test_goal():
        return

    x, y = algorithms.get_player_position()
    x_off, y_off, name = algorithms.get_player_direction()

    right_off = {
        'north': (1, 0),
        'east': (0, 1),
        'south': (-1, 0),
        'west': (0, -1),
    }

    right_x_off = right_off[name][0]
    right_y_off = right_off[name][1]
    while algorithms.test_wall(x + right_x_off, y + right_y_off) and not algorithms.test_wall(x + x_off, y + y_off):
        algorithms.move()
        x, y = algorithms.get_player_position()
        if algorithms.test_goal():
            return
