from server.impl.move_until import move_until
from server.model import algorithms


def right_turner():
    while not algorithms.test_goal():
        move_until()
        algorithms.turn_right()
