from server.impl.follow_wall import follow_wall
from server.model import algorithms


def wall_follower():
    follow_wall()
    while not algorithms.test_goal():
        algorithms.turn_right()
        follow_wall()
