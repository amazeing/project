from urllib.parse import urlparse

from flask import Flask, request
from flask_socketio import SocketIO

from server.events.navigation import Navigation
from server.events.tasks import Tasks

app = Flask('server', template_folder='static')
sio = SocketIO(app)

Navigation(sio)
task_events = Tasks(sio)


@app.route('/')
@app.route('/index.html')
@app.route('/index.htm')
def index():
    from flask import render_template

    o = urlparse(request.base_url)
    return render_template('index.html', hostname=o.hostname)


@app.route('/<path:path>')
def static_files(path):
    from flask import send_from_directory

    return send_from_directory(app.static_folder, path)


@app.route('/tasks/<tid>')
def tasks(tid=''):
    from flask import render_template

    o = urlparse(request.base_url)
    return render_template('tasks.html', tid=tid, hostname=o.hostname)


@app.route('/todo')
def todo():
    from flask import send_from_directory

    return send_from_directory(app.static_folder, 'todo.html')


@app.route('/credits')
def credit():
    from flask import send_from_directory

    return send_from_directory(app.static_folder, 'credits.html')
